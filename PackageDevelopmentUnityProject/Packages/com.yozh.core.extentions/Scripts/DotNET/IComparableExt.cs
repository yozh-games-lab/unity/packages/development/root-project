﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Core.APIExtentions.DotNET
{
	public static class IComparableExt
	{
		public static bool InRange<T>(this T value, T min, T max, bool inclusiveMin = true, bool inclusiveMax = true) where T : IComparable<T>
		{
			int minRef = inclusiveMin ? 0 : 1;
			int maxRef = inclusiveMax ? 0 : -1;
			return value.CompareTo(min) >= minRef && value.CompareTo(max) <= maxRef;
		}
	}
}