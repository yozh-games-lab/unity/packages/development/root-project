﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace YOZH.Core.APIExtentions.DotNET
{
	public static class LinkedListExt
	{
		public static LinkedListNode<T> NodeAt<T>(this LinkedList<T> list, int index)
		{
			if (!index.InRange(0, list.Count, true, false)) {
				throw new IndexOutOfRangeException();
			}

			int currentIndex = 0;
			LinkedListNode<T> currentNode = list.First;

			while (currentIndex < index && currentNode != null) {
				currentNode = currentNode.Next;
				currentIndex++;
			}

			return currentNode;
		}

		public static int IndexOf<T>(this LinkedList<T> list, T value)
		{
			int index = list.TakeWhile(item => !item.Equals(value)).Count();
			return index.InRange(0, list.Count - 1) ? index : -1;
		}
	}
}