﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace YOZH.Core.APIExtentions.DotNET
{
	public static class IEnumerableExt
	{
		public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> predicate)
		{
			foreach (T item in enumerable) {
				predicate.Invoke(item);
			}
		}

		public static int IndexOf<T>(this IEnumerable<T> enumerable, T target)
		{
			bool found = false;
			EqualityComparer<T> comparer = EqualityComparer<T>.Default;
			int count = enumerable.TakeWhile(item => !(found = comparer.Equals(target, item))).Count();
			return found ? count : -1;
		}
	}
}