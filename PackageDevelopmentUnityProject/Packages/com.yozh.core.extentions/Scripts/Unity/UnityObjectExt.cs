﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using YOZH.Core.APIExtentions.DotNET;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class UnityObjectExt
	{
		public static bool IsConvertibleTo(this UnityEngine.Object targetObject, Type targetType)
		{
			if (targetObject == null) return false;
			if (targetType == null) return false;

			if (targetType.IsAssignableFrom(targetObject.GetType())) return true;

			if (targetType.IsChildOf(typeof(Component)) || targetType.IsInterface) {
				Component component = targetObject as Component;
				if (component) return component.GetComponent(targetType);

				GameObject gameObject = targetObject as GameObject;
				if (gameObject) return gameObject.GetComponent(targetType);
			}

			return false;
		}

		public static T ConvertTo<T>(this UnityEngine.Object targetObject)
		{
			if (typeof(T).IsSubclassOf(typeof(Component)) || typeof(T).IsInterface) {
				Component component = targetObject as Component;
				if (component) return component.GetComponent<T>();

				GameObject gameObject = targetObject as GameObject;
				if (gameObject) return gameObject.GetComponent<T>();
			}

			return (T)(System.Object)targetObject;
		}

		public static UnityEngine.Object ConvertTo(this UnityEngine.Object targetObject, Type type)
		{
			if (type.IsSubclassOf(typeof(Component)) || type.IsInterface) {
				Component component = targetObject as Component;
				if (component) return component.GetComponent(type);

				GameObject gameObject = targetObject as GameObject;
				if (gameObject) return gameObject.GetComponent(type);
			}

			return targetObject;
		}

		public static void ValidateType<TSource, TTarget>(ref TSource source, string sourceName)
			where TSource : UnityEngine.Object
			where TTarget : class
		{
			if (source != null) {
				source = (source as TTarget) as TSource;
				Assert.IsNotNull(source, $"{sourceName} must implement {typeof(TTarget)}");
			}
		}

		public static TSource ValidateType<TSource, TTarget>(this TSource source, string sourceName)
			where TSource : UnityEngine.Object
			where TTarget : class
		{
			if (source != null) {
				source = (source as TTarget) as TSource;
				Assert.IsNotNull(source, $"{sourceName} must implement {typeof(TTarget)}");
			}

			return source;
		}
	}
}