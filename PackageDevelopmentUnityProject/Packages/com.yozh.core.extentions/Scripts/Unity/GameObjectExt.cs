﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Assertions;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class GameObjectExt
	{
		public static bool CompareTags(this GameObject gameobject, string[] tags)
		{
			foreach (string tag in tags) {
				if (gameobject.CompareTag(tag)) {
					return true;
				}
			}
			return false;
		}

		public static bool CompareTags(this GameObject gameobject, List<string> tags)
		{
			foreach (string tag in tags) {
				if (gameobject.CompareTag(tag)) {
					return true;
				}
			}
			return false;
		}

		public static GameObject ValidateScene(this GameObject target, Scene scene)
		{
			GameObject result = null;

			if (target != null) {
				result = target.scene == scene ? target : null;
				Assert.IsNotNull(result, $"{target.name} must be on scene {scene.name}");
			}

			return result;
		}

		public static bool IsPrefab(this GameObject target)
		{
			return target.scene.Equals(default(Scene));
		}
	}
}