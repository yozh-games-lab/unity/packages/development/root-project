﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;
using YOZH.Core.APIExtentions.DotNET;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class ComponentExt
	{
		public static TSource ValidateScene<TSource>(this TSource component, Scene scene) where TSource : Component
		{
			if (component != null) {
				component = component.gameObject.ValidateScene(scene) != null ? component : default(TSource);
			}

			return component;
		}

		public static T CloneTo<T>(this T original, GameObject destination) where T : Component
		{
			Type type = original.GetType();
			T clone = destination.GetComponent(type) as T ?? destination.AddComponent(type) as T;
			BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

			while (type != typeof(UnityEngine.Object)) {
				FieldInfo[] fields = type.GetFields(flags);
				fields.ForEach(item => item.SetValue(clone, item.GetValue(original)));
				type = type.BaseType;
			}

			return clone as T;
		}

		public static bool IsPrefab(this Component component) => component.gameObject.IsPrefab();
	}
}