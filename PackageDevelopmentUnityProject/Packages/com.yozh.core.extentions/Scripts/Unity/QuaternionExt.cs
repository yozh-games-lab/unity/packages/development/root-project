﻿using UnityEngine;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class QuaternionExt
	{
		public static Vector4 ToVector4(this Quaternion quaternion)
		{
			return new Vector4(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
		}

		public static Quaternion FromVector4(this Quaternion quaternion, Vector4 vector)
		{
			quaternion.x = vector.x;
			quaternion.y = vector.y;
			quaternion.z = vector.z;
			quaternion.w = vector.w;
			return quaternion;
		}

		public static Quaternion Round(this Quaternion quaternion, int accuracy)
		{
			return new Quaternion().FromVector4(quaternion.ToVector4().Round(accuracy));
		}
	}
}