﻿using System.IO;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class AssetDatabaseUtility
	{
		public static string GetResourceLoadPath(string assetPath)
		{
			if (string.IsNullOrEmpty(assetPath)) return null;

			assetPath = assetPath.Replace(@"\", @"/");
			if (!assetPath.StartsWith("Assets/")) return null;
			if (!assetPath.Contains("/Resources/")) return null;

			string result = Path.GetFileNameWithoutExtension(assetPath);
			string dirName = Path.GetDirectoryName(assetPath);

			while (!string.IsNullOrEmpty(dirName)) {
				string currentDir = Path.GetFileName(dirName);
				if (currentDir == "Resources") break;

				result = Path.Combine(currentDir, result);
				dirName = Path.GetDirectoryName(dirName);
			}
			result = result.Replace(@"\", @"/");

			return result;
		}
	}
}