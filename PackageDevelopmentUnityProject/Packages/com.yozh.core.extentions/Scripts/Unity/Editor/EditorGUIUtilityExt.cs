﻿using System;
using UnityEngine;
using UnityEditor;

namespace YOZH.Core.APIExtentions.Unity.Editor
{
	public class EditorGUIUtilityExt
	{
		public static UnityEngine.Object InterfaceField(Rect position, GUIContent label, UnityEngine.Object obj, Type baseType, Type interfaceType, bool allowSceneObject = false)
		{
			Event currentEvent = Event.current;

			float leftShift = 0;
			float labelWidth = string.IsNullOrEmpty(label.text) ? 0 : EditorGUIUtility.labelWidth;
			Rect labelPos = new Rect(position.x, position.y, labelWidth, position.height);

			float rightShift = position.height;
			leftShift += labelPos.width;
			Rect fieldPos = new Rect(position.x + leftShift, position.y, position.width - leftShift - rightShift, position.height);

			leftShift += fieldPos.width;
			Rect clearPos = new Rect(position.x + leftShift, position.y, rightShift, position.height);


			if (fieldPos.Contains(currentEvent.mousePosition)) {
				UnityEngine.Object draggedObject = (DragAndDrop.objectReferences.Length > 0) ? DragAndDrop.objectReferences[0] : null;
				if (draggedObject) {
					if (currentEvent.type == EventType.DragUpdated) {
						if (IsCorrectType(draggedObject, baseType, interfaceType) && IsAllowedObjectReference(draggedObject, allowSceneObject)) {
							DragAndDrop.visualMode = DragAndDropVisualMode.Link;
							obj = draggedObject.ConvertTo(interfaceType);
						}
						else {
							DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
							obj = null;
						}
					}
					if (currentEvent.type == EventType.DragPerform) obj = draggedObject.ConvertTo(interfaceType);
				}
			}

			GUIStyle labelStyle = GUI.skin.label;
			labelStyle.alignment = TextAnchor.MiddleLeft;
			EditorGUI.LabelField(labelPos, label, labelStyle);

			GUIContent fieldLabel = null;
			if (obj) {
				fieldLabel = new GUIContent(obj.name, AssetPreview.GetMiniThumbnail(obj));
			}
			else {
				fieldLabel = new GUIContent(string.Format("None ({0})", interfaceType.Name));
			}

			GUIStyle fieldStyle = GUI.skin.GetStyle("TextFieldDropDownText");
			if (GUI.Button(fieldPos, fieldLabel, fieldStyle)) {
				EditorGUIUtility.PingObject(obj);
			}

			GUIContent clearLabel = new GUIContent(EditorGUIUtility.IconContent("d_winbtn_mac_close_h").image);
			GUIStyle clearStyle = GUI.skin.label;
			clearStyle.alignment = TextAnchor.MiddleCenter;
			if (GUI.Button(clearPos, clearLabel, clearStyle)) obj = null;

			return obj;
		}

		public static bool IsCorrectType(UnityEngine.Object obj, Type baseType, Type interfaceType)
		{
			return obj.IsConvertibleTo(baseType) && obj.IsConvertibleTo(interfaceType);
		}

		public static bool IsAllowedObjectReference(UnityEngine.Object obj, bool allowSceneObject)
		{
			return (allowSceneObject || AssetDatabase.Contains(obj));
		}
	}
}