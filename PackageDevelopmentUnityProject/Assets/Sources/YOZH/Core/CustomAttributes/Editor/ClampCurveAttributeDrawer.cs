﻿using UnityEditor;
using UnityEngine;

namespace YOZH.Core.CustomAttributes.Editor
{
	[CustomPropertyDrawer(typeof(ClampCurveAttribute))]
	public class ClampCurveAttributeDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			ClampCurveAttribute clamp = attribute as ClampCurveAttribute;

			AnimationCurve curve = property.animationCurveValue;
			curve = EditorGUI.CurveField(position, label, curve, Color.white, clamp.Range);

			// for (int index = 0; index < curve.length; index++)
			// {
			//     AnimationUtility.SetKeyLeftTangentMode(curve, index, AnimationUtility.TangentMode.Linear);
			//     AnimationUtility.SetKeyRightTangentMode(curve, index, AnimationUtility.TangentMode.Linear);
			// }

			property.animationCurveValue = curve;
		}
	}
}