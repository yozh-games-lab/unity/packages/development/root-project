﻿using UnityEngine;
using System.Collections;


namespace YOZH.Core.CustomAttributes
{
	public class PopupAttribute : PropertyAttribute
	{
		public string[] Choices { get; protected set; }

		public PopupAttribute(params string[] choices)
		{
			Choices = choices;
		}
	}
}