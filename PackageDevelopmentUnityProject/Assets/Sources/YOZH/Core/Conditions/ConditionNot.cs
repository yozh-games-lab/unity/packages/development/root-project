﻿namespace YOZH.Core.Conditions
{
	internal sealed class ConditionNot : SingleCompositeCondition
	{
		protected override bool IsTrue(ICondition subCondition)
		{
			return !subCondition.IsTrue();
		}
	}
}