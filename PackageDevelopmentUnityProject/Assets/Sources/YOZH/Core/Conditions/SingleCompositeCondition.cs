﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using YOZH.Core.CustomAttributes;

namespace YOZH.Core.Conditions
{
	public abstract class SingleCompositeCondition : CompositeCondition
	{
		[SerializeField, InterfaceField(typeof(ICondition), allowSceneObject: true)]
		private Component SubCondition = null;

		protected override IEnumerable<ICondition> GetValidSubConditions()
		{
			if (SubCondition != null) {
				yield return SubCondition as ICondition;
			}
		}

		public override bool IsTrue()
		{
			return Count > 0 && IsTrue(GetValidSubConditions().First());
		}

		protected abstract bool IsTrue(ICondition subCondition);
	}
}