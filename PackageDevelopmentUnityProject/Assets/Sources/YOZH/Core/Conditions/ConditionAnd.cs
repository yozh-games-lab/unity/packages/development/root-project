﻿using System.Collections.Generic;
using System.Linq;

namespace YOZH.Core.Conditions
{
	internal sealed class ConditionAnd : MultiCompositeCondition
	{
		protected override bool IsTrue(IEnumerable<ICondition> subConditions)
		{
			return subConditions.All(item => item.IsTrue());
		}
	}
}
