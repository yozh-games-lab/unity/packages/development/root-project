﻿using UnityEngine;
using YOZH.Core.CustomAttributes;

namespace YOZH.Core.Conditions
{
	public class ConditionProxy : MonoBehaviour, ICondition
	{
		[SerializeField, InterfaceField(typeof(ICondition), allowSceneObject: true)]
		private Component ConditionComponent = null;

		private ICondition Condition => ConditionComponent as ICondition;

		bool ICondition.IsTrue()
		{
			return Condition.IsTrue();
		}
	}
}