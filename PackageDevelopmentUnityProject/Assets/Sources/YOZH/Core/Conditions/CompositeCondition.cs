﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace YOZH.Core.Conditions
{
	public abstract class CompositeCondition : MonoBehaviour, IReadOnlyCollection<ICondition>, ICondition
	{
		public int Count => GetValidSubConditions().Count();

		public IEnumerator<ICondition> GetEnumerator() => GetValidSubConditions().GetEnumerator();

		public abstract bool IsTrue();
		protected abstract IEnumerable<ICondition> GetValidSubConditions();

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}