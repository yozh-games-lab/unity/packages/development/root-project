﻿namespace YOZH.Core.Conditions
{
	public interface ICondition
	{
		bool IsTrue();
	}
}