﻿using System.Collections;
using UnityEngine;

namespace YOZH.Core.Coroutines
{
	public class BreakableInstruction : CustomYieldInstruction
	{
		private IEnumerator Instruction;
		private bool IsBroken;

		public override bool keepWaiting => !IsBroken && Instruction.MoveNext();

		public BreakableInstruction(IEnumerator instruction)
		{
			Instruction = instruction;
		}

		public void Break() => IsBroken = true;

		public new void Reset()
		{
			IsBroken = false;
			Instruction.Reset();
		}
	}
}