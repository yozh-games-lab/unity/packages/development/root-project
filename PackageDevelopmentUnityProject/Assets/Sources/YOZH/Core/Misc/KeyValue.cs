﻿using UnityEngine;
using System;

namespace YOZH.Core.Misc
{
	[Serializable]
	public class KeyValue<TKey, TValue>
	{
		[SerializeField]
		protected TKey key = default(TKey);
		public TKey Key { get { return key; } }

		[SerializeField]
		protected TValue value = default(TValue);
		public TValue Value { get { return value; } }

		public KeyValue(TKey key, TValue value)
		{
			this.key = key;
			this.value = value;
		}

		public override string ToString()
		{
			return string.Format("[{0}] {1}", key, value);
		}
	}
}