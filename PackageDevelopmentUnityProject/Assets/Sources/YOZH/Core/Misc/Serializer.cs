﻿using System;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace YOZH.Core.Misc
{
	public static class Serializer
	{

		public static string ConvertObjectToString(object obj, string password)
		{
			return convertObjectToString(obj, password);
		}
		public static string ConvertObjectToString(object obj)
		{
			return convertObjectToString(obj, string.Empty);
		}
		private static string convertObjectToString(object obj, string password)
		{
			if (!obj.GetType().IsSerializable) {
				throw new ArgumentException(string.Format("{0} is not serializable.", obj.GetType()));
			}

			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream memoryStream = new MemoryStream();
			binaryFormatter.Serialize(memoryStream, obj);

			string streamContent = Convert.ToBase64String(memoryStream.ToArray());
			if (!string.IsNullOrEmpty(password)) {
				streamContent = AesEncryption.Encrypt(streamContent, password);
			}
			return streamContent;
		}

		public static bool TryConvertStringToObject<T>(string content, string password, out T result)
		{
			return tryConvertStringToObject<T>(content, password, out result);
		}
		public static bool TryConvertStringToObject<T>(string content, out T result)
		{
			return tryConvertStringToObject<T>(content, string.Empty, out result);
		}
		private static bool tryConvertStringToObject<T>(string content, string password, out T result)
		{
			try {
				if (!string.IsNullOrEmpty(password)) {
					AesEncryption.TryDecrypt(content, password, out content);
				}

				BinaryFormatter binaryFormatter = new BinaryFormatter();
				byte[] contentBytes = Convert.FromBase64String(content);

				MemoryStream sr = new MemoryStream(contentBytes);
				result = (T)binaryFormatter.Deserialize(sr);

				if (result.Equals(default(T))) {
					return false;
				}
				return true;
			}
			catch (Exception) {
				result = default(T);
			}
			return false;
		}

		public static void Save(object obj, string fullPath, string password)
		{
			save(obj, fullPath, password);
		}
		public static void Save(object obj, string fullPath)
		{
			save(obj, fullPath, string.Empty);
		}
		private static void save(object obj, string fullPath, string password)
		{
			string directory = Path.GetDirectoryName(fullPath);
			if (!Directory.Exists(directory)) {
				Directory.CreateDirectory(directory);
			}

			string objContent = convertObjectToString(obj, password);
			byte[] objBytes = Encoding.UTF8.GetBytes(objContent);
			FileStream fileStream = File.Create(fullPath);
			fileStream.Write(objBytes, 0, objBytes.Length);
			fileStream.Flush();
			fileStream.Close();
		}

		public static bool TryLoad<T>(string fullPath, string password, out T result)
		{
			return tryLoad<T>(fullPath, password, out result);
		}
		public static bool TryLoad<T>(string fullPath, out T result)
		{
			return tryLoad<T>(fullPath, string.Empty, out result);
		}
		private static bool tryLoad<T>(string fullPath, string password, out T result)
		{
			if (!File.Exists(fullPath)) {
				result = default(T);
				return false;
			}

			StreamReader fileStream = new StreamReader(fullPath);
			string fileContent = fileStream.ReadToEnd();
			fileStream.Close();

			if (string.IsNullOrEmpty(fileContent)) {
				Debug.LogError(string.Format("Can't load file\n\"{0}\"", fullPath));
				result = default(T);
				return false;
			}

			if (!tryConvertStringToObject<T>(fileContent, password, out result)) {
				Debug.LogError(string.Format("File is empty or doesn't contains appropriate data or password is invalid\n\"{0}\"", fullPath));
				result = default(T);
				return false;
			}
			return true;
		}

	}
}