﻿using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace YOZH.Core.Misc
{
	public static class XmlUtils
	{
		public static string ToXMLString(this object instance)
		{
			XmlSerializer serializer = new XmlSerializer(instance.GetType());
			StringBuilder stringBuilder = new StringBuilder();

			using (TextWriter writer = new StringWriter(stringBuilder)) {
				serializer.Serialize(writer, instance);
			}
			return stringBuilder.ToString();
		}

		public static T ParceXML<T>(this string objectData)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));
			object obj;

			using (TextReader stream = new StringReader(objectData)) {
				obj = serializer.Deserialize(stream);
			}

			return (T)obj;
		}

		public static bool TryParceXML<T>(this string objectData, out T result)
		{
			result = objectData.ParceXML<T>();
			return !result.Equals(default(T));
		}
	}
}