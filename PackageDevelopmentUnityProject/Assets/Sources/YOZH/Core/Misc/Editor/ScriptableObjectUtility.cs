﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace YOZH.Core.Misc.Editor
{

	public class ScriptableObjectUtility : UnityEditor.Editor
	{
		/// <summary>
		/// Makes an empty scriptable object in selected folder and selects it.
		/// </summary>
		[MenuItem("Assets/Create/Empty Scriptable Object")]
		public static void CreateEmptyScriptableObject()
		{
			ScriptableObject obj = CreateInstance<ScriptableObject>();
			AssetDatabase.CreateAsset(obj, AssetDatabase.GetAssetPath(Selection.activeObject).ToString() + "/NewScriptableObject.asset");
			AssetDatabase.SaveAssets();

			Selection.activeObject = obj;

			Event e = new Event() { keyCode = KeyCode.F2, type = EventType.KeyDown };
			EditorUtility.FocusProjectWindow();
			EditorWindow.focusedWindow.SendEvent(e);
		}

		/// <summary>
		/// path - Some path relative to Assets folder. Eg. "ScriptableObjects/Somefolders"
		/// autoBrowse - If the method should automaticaly navigate the project window to result.
		/// If target asset exists it will return the existing one.
		/// </summary>
		public static T CreateScriptableObjectAtPath<T>(string path, bool autoBrowse = false) where T : ScriptableObject
		{
			path = MiscUtilities.NormalizePath(path, "Assets", ".asset");

			ScriptableObject output = AssetDatabase.LoadAssetAtPath<T>(path);

			if (output == null) {
				string dir = Path.GetDirectoryName(path);
				if (!Directory.Exists(dir)) {
					Directory.CreateDirectory(dir);
					AssetDatabase.Refresh();
				}
				output = ScriptableObject.CreateInstance<T>();
				AssetDatabase.CreateAsset(output, path);
				AssetDatabase.SaveAssets();
			}
			if (autoBrowse) {
				Selection.activeObject = output;
			}

			return (T)output;
		}
	}
}