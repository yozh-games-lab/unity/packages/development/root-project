﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;


namespace YOZH.Core.Misc.Editor
{
	public class FindMissingScriptsRecursively : EditorWindow
	{
		static int go_count = 0, components_count = 0, missing_count = 0;

		[MenuItem("YOZH/Dev tools/Find missings on scene")]
		private static void FindInSelected()
		{
			GameObject[] targetObjects = SceneManager.GetActiveScene().GetRootGameObjects();
			go_count = 0;
			components_count = 0;
			missing_count = 0;

			Debug.Log("---------------------------------------------------");
			foreach (GameObject obj in targetObjects) {
				FindInGO(obj);
			}
			Debug.Log(string.Format("[ Searched {0} GameObjects, {1} components, found {2} missing ]",
									go_count, components_count, missing_count));
			Debug.Log("---------------------------------------------------");
		}

		private static void FindInGO(GameObject g)
		{
			go_count++;
			Component[] components = g.GetComponents<Component>();
			for (int i = 0; i < components.Length; i++) {
				components_count++;
				if (components[i] == null) {
					missing_count++;
					string currentObjName = g.name;
					Transform currentObjTransform = g.transform;
					while (currentObjTransform.parent != null) {
						currentObjName = currentObjTransform.parent.name + "/" + currentObjName;
						currentObjTransform = currentObjTransform.parent;
					}
					Debug.Log(currentObjName + " has an empty script attached in position: " + i, g);
				}
			}

			foreach (Transform childT in g.transform) {
				FindInGO(childT.gameObject);
			}
		}
	}
}