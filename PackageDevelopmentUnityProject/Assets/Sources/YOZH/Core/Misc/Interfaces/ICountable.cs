namespace YOZH.Core.Misc.Interfaces
{
	public interface ICountable
	{
		uint Count { get; }
	}
}