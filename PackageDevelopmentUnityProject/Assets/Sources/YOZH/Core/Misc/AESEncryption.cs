﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;


namespace YOZH.Core.Misc
{
	public static class AesEncryption
	{
		private const int ITERATIONS = 1000;

		public static string Encrypt(string content, string password)
		{
			if (string.IsNullOrEmpty(content)) {
				throw new ArgumentNullException("[AESEncryption] Trying encrypt empty content");
			}

			if (string.IsNullOrEmpty(password)) {
				throw new ArgumentNullException("[AESEncryption] Trying encrypt content without password");
			}

			// create instance of the DES crypto provider
			DESCryptoServiceProvider des = new DESCryptoServiceProvider();

			// generate a random IV will be used a salt value for generating key
			des.GenerateIV();

			// use derive bytes to generate a key from the password and IV
			Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, des.IV, ITERATIONS);

			// generate a key from the password provided
			byte[] key = rfc2898DeriveBytes.GetBytes(8);

			// encrypt the decryptedContent
			using (MemoryStream memoryStream = new MemoryStream()) {
				using (
					CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(key, des.IV),
																 CryptoStreamMode.Write)) {
					// write the salt first not encrypted
					memoryStream.Write(des.IV, 0, des.IV.Length);

					// convert the plain text string into a byte array
					byte[] bytes = Encoding.UTF8.GetBytes(content);

					// write the bytes into the crypto stream so that they are encrypted bytes
					cryptoStream.Write(bytes, 0, bytes.Length);
					cryptoStream.FlushFinalBlock();

					return Convert.ToBase64String(memoryStream.ToArray());
				}
			}
		}

		public static bool TryDecrypt(string content, string password, out string decryptedContent)
		{
			if (string.IsNullOrEmpty(content) || string.IsNullOrEmpty(password)) {
				decryptedContent = string.Empty;
				return false;
			}

			byte[] cipherBytes = Convert.FromBase64String(content);

			using (MemoryStream memoryStream = new MemoryStream(cipherBytes)) {
				// create instance of the DES crypto provider
				DESCryptoServiceProvider des = new DESCryptoServiceProvider();

				// get the IV
				byte[] iv = new byte[8];
				memoryStream.Read(iv, 0, iv.Length);

				// use derive bytes to generate key from password and IV
				Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, iv, ITERATIONS);

				byte[] key = rfc2898DeriveBytes.GetBytes(8);

				using (
					CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(key, iv),
																 CryptoStreamMode.Read)) {
					using (StreamReader streamReader = new StreamReader(cryptoStream)) {
						decryptedContent = streamReader.ReadToEnd();
						return true;
					}
				}
			}
		}
	}
}