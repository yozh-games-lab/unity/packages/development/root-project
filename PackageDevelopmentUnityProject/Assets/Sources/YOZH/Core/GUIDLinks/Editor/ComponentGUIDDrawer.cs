﻿using UnityEngine;

namespace YOZH.Core.GUIDLinks.Editor
{
	public abstract class ComponentGUIDDrawer<T> : GUIDLinkPropertyDrawer<T> where T : Component
	{
	}
}