﻿using UnityEditor;
using YOZH.Core.UnityEvents.Editor;

namespace YOZH.Core.Properties.Editor
{
	[CustomPropertyDrawer(typeof(IntProperty.ChangeEvent), true)]
	[CustomPropertyDrawer(typeof(FloatProperty.ChangeEvent), true)]
	[CustomPropertyDrawer(typeof(BoolProperty.ChangeEvent), true)]
	[CustomPropertyDrawer(typeof(StringProperty.ChangeEvent), true)]
	public class BasePropertyChangeEventDrawer : UnityEventIndentedDrawer
	{
	}
}