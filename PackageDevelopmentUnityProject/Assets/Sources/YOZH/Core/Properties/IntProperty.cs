﻿using System;
using UnityEngine.Events;

namespace YOZH.Core.Properties
{
	[Serializable]
	public class IntProperty : BaseProperty<int, IntProperty.ChangeEvent>
	{
		public IntProperty() : base()
		{
		}

		public IntProperty(int value) : base(value)
		{
		}

		[Serializable]
		public class ChangeEvent : UnityEvent<int, int>
		{
		}
	}
}