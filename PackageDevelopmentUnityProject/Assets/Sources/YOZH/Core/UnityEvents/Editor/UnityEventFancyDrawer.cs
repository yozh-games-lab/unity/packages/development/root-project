﻿using System.Reflection;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.UnityEvents.Editor
{
	[CustomPropertyDrawer(typeof(UnityEventBase), true)]
	public class UnityEventFancyDrawer : UnityEventDrawer
	{
		private SerializedProperty _property = default;
		private Rect _expandRect = default;

		private ReorderableList reorderableList
			=> GetType().BaseType
				.GetField("m_ReorderableList", BindingFlags.NonPublic | BindingFlags.Instance)
				.GetValue(this) as ReorderableList;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			_property = property;

			label.text = $"{(property.isExpanded ? "▼" : "►")} {label.text}";
			Rect intendedRect = EditorGUI.IndentedRect(position);
			_expandRect = new Rect(intendedRect.position, GUI.skin.label.CalcSize(label));
			base.OnGUI(intendedRect, property, label);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			RestoreState(property);
			if (reorderableList != default)
			{
				if (property.isExpanded)
				{
					SetupReorderableList(reorderableList);
				}
				else
				{
					reorderableList.elementHeight = 0;
				}
			}
			return base.GetPropertyHeight(property, label);
		}

		protected override void DrawEventHeader(Rect rect)
		{
			if (GUI.Button(_expandRect, GUIContent.none, GUIStyle.none))
			{
				_property.isExpanded = !_property.isExpanded;
			}

			base.DrawEventHeader(rect);
		}

		protected override void DrawEvent(Rect rect, int index, bool isActive, bool isFocused)
		{
			if (_property.isExpanded)
			{
				base.DrawEvent(rect, index, isActive, isFocused);
			}
		}

		private void RestoreState(SerializedProperty property)
		{
			GetType().BaseType
				.GetMethod("RestoreState", BindingFlags.NonPublic | BindingFlags.Instance)
				.Invoke(this, new object[] {property});
		}
	}
}