﻿using UnityEngine;

namespace YOZH.Core.UnityEvents.AnimatorEvents
{
	public class AnimatorStateHandler : MonoBehaviour, IAnimatorEventBehaviour
	{
		[SerializeField] private AnimatorStateEvents State = new AnimatorStateEvents();

		void IAnimatorEventBehaviour.OnStateEnter(AnimatorStateInfo stateInfo, int layerIndex)
		{
			State.RaiseOnStateEnter(stateInfo);
		}

		void IAnimatorEventBehaviour.OnStateExit(AnimatorStateInfo stateInfo, int layerIndex)
		{
			State.RaiseOnStateExit(stateInfo);
		}

		void IAnimatorEventBehaviour.OnStateIK(AnimatorStateInfo stateInfo, int layerIndex)
		{
			State.RaiseOnStateIK(stateInfo);
		}

		void IAnimatorEventBehaviour.OnStateMove(AnimatorStateInfo stateInfo, int layerIndex)
		{
			State.RaiseOnStateMove(stateInfo);
		}

		void IAnimatorEventBehaviour.OnStateUpdate(AnimatorStateInfo stateInfo, int layerIndex)
		{
			State.RaiseOnStateUpdate(stateInfo);
		}
	}
}