﻿using UnityEditor;
using UnityEngine;
using YOZH.Core.APIExtentions.Unity.Editor;

namespace YOZH.Core.Arithmetics.Editor
{
	[CustomEditor(typeof(BaseCalculatorBehaviour), true)]
	public class BaseCalculatorBehaviourEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			this.DrawDefaultScriptTitle(serializedObject);

			SerializedProperty argsEventProperty = serializedObject.FindProperty("OnSetArgumentsEvent");
			SerializedProperty outputEventProperty = serializedObject.FindProperty("OnResultEvent");

			string[] exceptionList = new string[]
			{
				EditorDrawerUtility.ScriptFieldName, outputEventProperty.name, argsEventProperty.name,
			};
			DrawPropertiesExcluding(serializedObject, exceptionList);

			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(argsEventProperty);
			EditorGUILayout.PropertyField(outputEventProperty);

			serializedObject.ApplyModifiedProperties();
			DrawButtons();
		}

		private void DrawPropertiesExcept(
			SerializedProperty arg1EventProperty,
			SerializedProperty arg2EventProperty,
			SerializedProperty outputEventProperty)
		{
		}

		private void DrawButtons()
		{
			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal();
			BaseCalculatorBehaviour calculator = target as BaseCalculatorBehaviour;
			if (GUILayout.Button(nameof(BaseCalculatorBehaviour.Sum))) calculator.Sum();
			if (GUILayout.Button(nameof(BaseCalculatorBehaviour.Substract))) calculator.Substract();
			if (GUILayout.Button(nameof(BaseCalculatorBehaviour.Multiply))) calculator.Multiply();
			if (GUILayout.Button(nameof(BaseCalculatorBehaviour.Divide))) calculator.Divide();
			EditorGUILayout.EndHorizontal();
		}
	}
}