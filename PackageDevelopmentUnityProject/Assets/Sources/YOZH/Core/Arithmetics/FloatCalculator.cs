﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.Arithmetics
{
	public class FloatCalculator : CalculatorBehaviour<float, FloatCalculator.ResultEvent>
	{
		[SerializeField] protected ResultEvent OnResultEvent = null;

		protected override float Sum(float arg1, float arg2) => arg1 + arg2;
		protected override float Substract(float arg1, float arg2) => arg1 - arg2;
		protected override float Multiply(float arg1, float arg2) => arg1 * arg2;
		protected override float Divide(float arg1, float arg2) => arg1 / arg2;

		protected override ResultEvent GetOnResultEvent() => OnResultEvent;

		[Serializable] public class ResultEvent : UnityEvent<float> { }
	}
}