﻿using UnityEngine;

namespace YOZH.Plugins.InputManager
{
	public class TestInputAgent : InputManagerAgent
	{
		protected override void onEscape()
		{
			Debug.Log("Presed escape key");
		}

		protected override void onClick(Vector2 screenPoint)
		{
			Debug.Log(string.Format("Clicked at pos {0}", screenPoint));
		}

		protected override void onSwipe(Vector2 startPoint, Vector2 endPoint)
		{
			Debug.Log(string.Format("Swiped from {0} to {1}", startPoint, endPoint));
		}

		protected override void onHoldStationary(Vector2 screenPoint, float holdTime)
		{
			Debug.Log(string.Format("Hold click at pos {0} for time {1}", screenPoint, holdTime));
		}
		protected override void onDoubleClick(Vector2 screenPoint)
		{
			Debug.Log(string.Format("Double clicked at pos {0}", screenPoint));
		}
	}
}