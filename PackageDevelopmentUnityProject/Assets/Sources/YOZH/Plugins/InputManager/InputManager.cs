﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace YOZH.Plugins.InputManager
{
	public class InputManager : MonoBehaviour
	{
		#region Singleton
		private static InputManager instance = null;
		public static InputManager Instance {
			get {
				if (instance == null) instance = createInstance();
				return instance;
			}
		}
		private void init()
		{
			if (instance == null) instance = this;
			else Destroy(this);
		}
		private static InputManager createInstance()
		{
			InputManager result = FindObjectOfType<InputManager>();
			if (result == null) {
				GameObject go = new GameObject();
				result = go.AddComponent<InputManager>();
				go.name = result.GetType().Name;
			}
			return result;
		}
		#endregion

		#region Events
		public delegate void VoidInputEvent();

		private static event VoidInputEvent onEscape = null;
		public static event VoidInputEvent OnEscape {
			add {
				if (instance == null) createInstance();
				onEscape += value;
			}
			remove { onEscape -= value; }
		}
		private void raiseOnEscape()
		{
			if (onEscape != null) onEscape();
		}
		public void RaiseOnEscape()
		{
			raiseOnEscape();
		}

		public delegate void ClickInputEvent(Vector2 screenPoint);
		private static event ClickInputEvent onClick = null;
		public static event ClickInputEvent OnClick {
			add {
				if (instance == null) createInstance();
				onClick += value;
			}
			remove { onClick -= value; }
		}
		private void raiseOnClick(Vector2 screenPoint)
		{
			if (onClick != null) onClick(screenPoint);
		}
		public void RaiseOnClick(Vector2 screenPoint)
		{
			raiseOnClick(screenPoint);
		}

		private static event ClickInputEvent onClickDown = null;
		public static event ClickInputEvent OnClickDown {
			add {
				if (instance == null) createInstance();
				onClickDown += value;
			}
			remove { onClickDown -= value; }
		}
		private void raiseOnClickDown(Vector2 screenPoint)
		{
			if (onClickDown != null) onClickDown(screenPoint);
		}
		public void RaiseOnClickDown(Vector2 screenPoint)
		{
			raiseOnClickDown(screenPoint);
		}

		private static event ClickInputEvent onClickUp = null;
		public static event ClickInputEvent OnClickUp {
			add {
				if (instance == null) createInstance();
				onClickUp += value;
			}
			remove { onClickUp -= value; }
		}
		private void raiseOnClickUp(Vector2 screenPoint)
		{
			if (onClickUp != null) onClickUp(screenPoint);
		}
		public void RaiseOnClickUp(Vector2 screenPoint)
		{
			raiseOnClickUp(screenPoint);
		}

		private static event ClickInputEvent onMouseMove = null;
		public static event ClickInputEvent OnMouseMove {
			add {
				if (instance == null) createInstance();
				onMouseMove += value;
			}
			remove { onMouseMove -= value; }
		}
		private void raiseOnMouseMove(Vector2 screenPoint)
		{
			if (onMouseMove != null) onMouseMove(screenPoint);
		}
		public void RaiseOnMouseMove(Vector2 screenPoint)
		{
			raiseOnMouseMove(screenPoint);
		}

		private static event ClickInputEvent onDoubleClick = null;
		public static event ClickInputEvent OnDoubleClick {
			add {
				if (instance == null) createInstance();
				onDoubleClick += value;
			}
			remove { onDoubleClick -= value; }
		}
		private void raiseOnDoubleClick(Vector2 screenPoint)
		{
			if (onDoubleClick != null) onDoubleClick(screenPoint);
		}
		public void RaiseOnDoubleClick(Vector2 screenPoint)
		{
			raiseOnDoubleClick(screenPoint);
		}

		public delegate void SwipeInputEvent(Vector2 startPoint, Vector2 endPoint);
		private static event SwipeInputEvent onSwipe = null;
		public static event SwipeInputEvent OnSwipe {
			add {
				if (instance == null) createInstance();
				onSwipe += value;
			}
			remove { onSwipe -= value; }
		}
		private void raiseOnSwipe(Vector2 startPoint, Vector2 endPoint)
		{
			if (onSwipe != null) onSwipe(startPoint, endPoint);
		}
		public void RaiseOnSwipe(Vector2 startPoint, Vector2 endPoint)
		{
			raiseOnSwipe(startPoint, endPoint);
		}

		public delegate void HoldClickInputEvent(Vector2 screenPoint, float holdTime);
		private static event HoldClickInputEvent onHoldStationary = null;
		public static event HoldClickInputEvent OnHoldStationaty {
			add {
				if (instance == null) createInstance();
				onHoldStationary += value;
			}
			remove { onHoldStationary -= value; }
		}
		private void raiseOnHoldStationary(Vector2 screenPoint, float holdTime)
		{
			if (onHoldStationary != null) onHoldStationary(screenPoint, holdTime);
		}
		public void RaiseOnHoldStationary(Vector2 screenPoint, float holdTime)
		{
			raiseOnHoldStationary(screenPoint, holdTime);
		}

		private static event HoldClickInputEvent onHold = null;
		public static event HoldClickInputEvent OnHold {
			add {
				if (instance == null) createInstance();
				onHold += value;
			}
			remove { onHold -= value; }
		}
		private void raiseOnHold(Vector2 screenPoint, float holdTime)
		{
			if (onHold != null) onHold(screenPoint, holdTime);
		}
		public void RaiseOnHold(Vector2 screenPoint, float holdTime)
		{
			raiseOnHold(screenPoint, holdTime);
		}
		#endregion

		private enum TimeMode
		{
			Scaled,
			Unscaled,
			Fixed

		}
		[SerializeField]
		private TimeMode timeMode = TimeMode.Scaled;
		private float currentTime {
			get {
				switch (timeMode) {
					case TimeMode.Fixed:
						return Time.fixedTime;
					case TimeMode.Unscaled:
						return Time.unscaledTime;
					case TimeMode.Scaled:
					default:
						return Time.time;
				}
			}
		}
		[SerializeField]
		private int moveErrorPix = 15;
		[SerializeField]
		private int baseDPI = 96;
		[SerializeField]
		private float moveError {
			get { return moveErrorPix * (float)baseDPI / Screen.dpi; }
		}
		[SerializeField]
		private float clickHoldDelay = 0.15f;
		[SerializeField]
		private float doubleClickSpeed = 0.2f;
		[SerializeField]
		private float doubleTouchSpeedFactor = 2;
		private float expectedDoubleTouchSpeed {
			get {
				switch (listenMode) {
					case ListenMode.Touch:
						return doubleClickSpeed * doubleTouchSpeedFactor;

					case ListenMode.Mouse:
					default:
						return doubleClickSpeed;
				}
			}
		}


		private enum ClickPhase
		{
			Begin,
			Hold,
			End

		}
		private ClickInfo currentClick = new ClickInfo();
		private int clicksCount = 0;
		private IEnumerator clickTimerRoutine = null;

		private enum ListenMode
		{
			Mouse,
			Touch

		}
		private ListenMode listenMode;

		[SerializeField]
		private bool TestTouchInput = false;
		public bool IsTouchScreen {
			get {
				if (Instance.TestTouchInput || SystemInfo.deviceType == DeviceType.Handheld) {
					return true;
				}
				else return false;
			}
		}

		private Dictionary<string, Joystick> joysticks = new Dictionary<string, Joystick>();

		public static Joystick GetJoystick(string tag)
		{
			if (instance == null) return null;

			Joystick result = null;
			instance.joysticks.TryGetValue(tag, out result);
			return result;
		}
		public static void AddJoystick(string key, Joystick joystick)
		{
			if (instance == null) return;

			if (!instance.joysticks.ContainsKey(key)) {
				instance.joysticks.Add(key, joystick);
			}
		}
		public static void RemoveJoyStick(string key)
		{
			if (instance == null) return;

			instance.joysticks.Remove(key);
		}

		private void Awake()
		{
			init();
			DontDestroyOnLoad(this);
		}
		private void Update()
		{
			if (timeMode == TimeMode.Scaled || timeMode == TimeMode.Unscaled) listenAllInput();
		}
		private void FixedUpdate()
		{
			if (timeMode == TimeMode.Fixed) listenAllInput();
		}

		private void listenAllInput()
		{
			if (!enabled) return;

			listenMouse();
			listenTouch();
			listenKeyboard();
		}
		private void listenMouse()
		{
#if !UNITY_EDITOR && !UNITY_STANDALONE
            if (!Input.mousePresent) return;
#endif
			listenMode = ListenMode.Mouse;

			if (UnityEngine.Input.GetMouseButtonDown(0)) clickDownHandler(Input.mousePosition);
			if (UnityEngine.Input.GetMouseButton(0)) clickHoldHandler(Input.mousePosition);
			if (UnityEngine.Input.GetMouseButtonUp(0)) clickUpHandler(Input.mousePosition);

			if (!IsTouchScreen) {
				if (Input.GetAxis("Mouse X") < 0 || Input.GetAxis("Mouse X") > 0) {
					mouseMoveHandler(Input.mousePosition);
				}
			}

		}
		private void listenTouch()
		{
			listenMode = ListenMode.Touch;

			if (UnityEngine.Input.touchCount > 0) {
				Touch touch = UnityEngine.Input.touches[0];

				if (touch.phase == TouchPhase.Began) clickDownHandler(touch.position);

				if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) clickHoldHandler(touch.position);

				if (touch.phase == TouchPhase.Ended) clickUpHandler(touch.position);
			}
		}
		private void listenKeyboard()
		{
			if (UnityEngine.Input.GetKeyUp(KeyCode.Escape)) raiseOnEscape();
		}

		private void clickDownHandler(Vector2 screenPoint)
		{
			raiseOnClickDown(screenPoint);

			if (currentClick.CurrentPhase == ClickPhase.End) currentClick.SetPhase(ClickPhase.Begin, screenPoint, currentTime);
		}
		private void clickHoldHandler(Vector2 screenPoint)
		{
			PhaseInfo beginPhase = currentClick.GetPhase(ClickPhase.Begin);

			currentClick.SetPhase(ClickPhase.Hold, screenPoint, currentTime);
			if (currentTime - beginPhase.Time > clickHoldDelay && Vector2.Distance(beginPhase.Position,
																				   UnityEngine.Input.mousePosition) < moveError) {
				raiseOnHoldStationary(screenPoint, currentTime - beginPhase.Time - clickHoldDelay);
			}
			else {
				raiseOnHold(screenPoint, currentTime - beginPhase.Time - clickHoldDelay);
			}
		}
		private void clickUpHandler(Vector2 screenPoint)
		{
			currentClick.SetPhase(ClickPhase.End, screenPoint, currentTime);

			float currentClickMoveDist = Vector2.Distance(currentClick.GetPhase(ClickPhase.Begin).Position,
														  currentClick.GetPhase(ClickPhase.End).Position);
			if (currentClickMoveDist > moveError) {
				raiseOnSwipe(currentClick.GetPhase(ClickPhase.Begin).Position, currentClick.GetPhase(ClickPhase.End).Position);
			}
			else {
				clicksCount++;
				if (clickTimerRoutine == null) {
					clickTimerRoutine = clickTimer(screenPoint);
					StartCoroutine(clickTimerRoutine);
				}
			}

			raiseOnClickUp(screenPoint);
		}
		private void mouseMoveHandler(Vector2 screenPoint)
		{
			raiseOnMouseMove(screenPoint);
		}
		private IEnumerator clickTimer(Vector2 screenPoint)
		{
			yield return new WaitForSeconds(expectedDoubleTouchSpeed);
			if (clicksCount > 1) {
				raiseOnDoubleClick(screenPoint);
			}
			else {
				raiseOnClick(screenPoint);
			}
			clicksCount = 0;
			clickTimerRoutine = null;
		}

		[Serializable]
		private class ClickInfo : ICloneable
		{
			private ClickPhase currentPhase = ClickPhase.End;
			public ClickPhase CurrentPhase { get { return currentPhase; } }

			private PhaseInfo beginPhase = new PhaseInfo();
			private PhaseInfo holdPhase = new PhaseInfo();
			private PhaseInfo endPhase = new PhaseInfo();

			public ClickInfo()
			{
			}
			private ClickInfo(ClickPhase currentPhase, PhaseInfo beginPhase, PhaseInfo holdPhase, PhaseInfo endPhase)
			{
				this.currentPhase = currentPhase;
				this.beginPhase = beginPhase.Clone() as PhaseInfo;
				this.holdPhase = holdPhase.Clone() as PhaseInfo;
				this.endPhase = endPhase.Clone() as PhaseInfo;
			}

			public void SetPhase(ClickPhase phase, Vector2 clickPoint, float currentTime)
			{
				currentPhase = phase;
				PhaseInfo targetInfo = null;

				switch (phase) {
					case ClickPhase.Begin:
						targetInfo = beginPhase;
						break;

					case ClickPhase.Hold:
						targetInfo = holdPhase;
						break;

					case ClickPhase.End:
						targetInfo = endPhase;
						break;
				}
				targetInfo.Position = clickPoint;
				targetInfo.Time = currentTime;

			}
			public PhaseInfo GetPhase(ClickPhase phase)
			{
				switch (phase) {
					case ClickPhase.Begin:
						return beginPhase;
					case ClickPhase.Hold:
						return holdPhase;
					case ClickPhase.End:
						return endPhase;
					default:
						return new PhaseInfo();
				}
			}
			public object Clone()
			{
				return new ClickInfo(currentPhase, beginPhase, holdPhase, endPhase);
			}
		}


		[Serializable]
		private class PhaseInfo : ICloneable
		{
			public Vector2 Position = -Vector2.one;
			public float Time = float.PositiveInfinity;

			public PhaseInfo()
			{
			}
			private PhaseInfo(Vector2 position, float time)
			{
				Position = position;
				Time = time;
			}

			public object Clone()
			{
				return new PhaseInfo(Position, Time);
			}
		}
	}
}