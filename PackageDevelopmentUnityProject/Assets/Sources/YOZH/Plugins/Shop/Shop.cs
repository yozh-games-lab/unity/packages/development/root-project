﻿using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.Shop
{
	public class Shop : ScriptableObject
	{
		public const string RESOURCE_PATH = "GameSettings/Shop/Shop";

		private static Shop instance = null;
		public static Shop Instance {
			get {
				if (!instance) instance = Resources.Load<Shop>(RESOURCE_PATH);
				return instance;
			}
		}

		[SerializeField]
		private Category rootCategory = null;
		public string[] Categories { get { return (rootCategory) ? rootCategory.ToList() : new string[0]; } }

		[SerializeField, GoodID]
		private List<string> currencies = new List<string>();
		public string[] Currencies { get { return currencies.ToArray(); } }

		public Category GetCategory(string path)
		{
			return (rootCategory) ? rootCategory.FindSubCategory(path) : null;
		}

		public void Buy(IProduct product, uint count, string currencyId, IShopCustomer customer)
		{
			ExchangeRate rate = null;
			if (!product.GetRate(currencyId, out rate)) return;

			IGood[] goods = new IGood[0];
			if (customer.TakeAway(rate.CurrencyId, rate.Price * count, out goods)) {
				int acceptedCount = 0;

				foreach (IGood good in goods) {
					good.Validate(isAccepted =>
					{
						if (isAccepted) {
							acceptedCount++;
							if (acceptedCount == goods.Length) {
								IGood sold = ShopStore.Instance.GetItem(product.GoodID, count);
								customer.Give(sold);
								foreach (IGood item in goods) item.Dispose();
							}
						}
						else foreach (IGood item in goods) customer.Give(item);
					});
				}

			}
		}

		public void Buy(string categoryID, string productId, uint count, string currencyId, IShopCustomer customer)
		{
			IProduct product = null;
			if (rootCategory.FindProductRecurcive(productId, out product)) {
				Buy(product, count, currencyId, customer);
			}
		}
	}
}