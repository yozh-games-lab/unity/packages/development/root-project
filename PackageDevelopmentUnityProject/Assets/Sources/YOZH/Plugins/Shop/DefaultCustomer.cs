﻿using UnityEngine;
using System.Collections.Generic;

namespace YOZH.Plugins.Shop
{
	public class DefaultCustomer : MonoBehaviour, IShopCustomer
	{
		[SerializeField]
		private Transform goodsContainer = null;

		private List<IGood> goods = new List<IGood>();

		private Transform getGoodsContainer()
		{
			if (!goodsContainer) goodsContainer = transform;
			return goodsContainer;
		}

		#region ICustomer implementation
		public void Give(IGood good)
		{
			goods.Add(good);
			Component goodObject = good as Component;
			if (goodObject) {
				goodObject.transform.SetParent(getGoodsContainer(), false);
				onBalanceChanged(this);
			}
		}

		public bool TakeAway(string goodID, uint count, out IGood[] result)
		{
			result = new IGood[0];
			List<IGood> search = goods.FindAll(item => item.ID == goodID);
			if (search.Count >= count) {
				search.RemoveRange((int)count, search.Count - (int)count);
				foreach (IGood good in search) {
					goods.Remove(good);
					Component goodObject = good as Component;
					if (goodObject) goodObject.transform.SetParent(null, false);
				}
				result = search.ToArray();
			}
			if (result.Length > 0) {
				onBalanceChanged(this);
				return true;
			}
			else return false;
		}

		public uint GetBalance(string goodID)
		{
			uint result = 0;
			goods.FindAll(item => item.ID == goodID).ForEach(item => result += item.Count);
			return result;
		}

		public event ShopCustomerEvent onBalanceChanged = (customer) => { };
		public event ShopCustomerEvent OnBalanceChanged {
			add { onBalanceChanged += value; }
			remove { onBalanceChanged -= value; }
		}
		#endregion

	}
}
