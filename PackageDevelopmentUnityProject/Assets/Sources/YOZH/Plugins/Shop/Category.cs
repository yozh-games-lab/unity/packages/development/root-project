﻿using YOZH.Core.CustomAttributes;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

namespace YOZH.Plugins.Shop
{
	[Serializable]
	public class Category : ShopItem
	{
		[SerializeField]
		private List<Category> subCategories = new List<Category>();
		public Category[] SubCategories { get { return subCategories.ToArray(); } }
		[SerializeField, InterfaceField(typeof(IProduct))]
		private List<ScriptableObject> products = new List<ScriptableObject>();
		public IProduct[] Products { get { return products.ConvertAll(item => item as IProduct).ToArray(); } }

		public Category FindSubCategory(string path)
		{
			string[] parts = path.Split(@"\/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

			if (parts.Length > 0 && parts[0] == ID) {
				if (parts.Length > 1) {
					Category subCategory = subCategories.Find(item => item.ID == parts[1]);
					if (subCategory != null) {
						string subPath = "";
						for (int index = 1; index < parts.Length; index++) {
							subPath = Path.Combine(subPath, parts[index]);
						}
						return subCategory.FindSubCategory(subPath);
					}
				}
				else {
					return this;
				}
			}
			return null;
		}

		public bool FindProductRecurcive(string productId, out IProduct result)
		{
			if (GetProduct(productId, out result)) {
				return true;
			}
			foreach (Category category in subCategories) {
				if (category.FindProductRecurcive(productId, out result)) {
					return true;
				}
			}
			result = null;
			return false;
		}

		public bool GetProduct(string productId, out IProduct result)
		{
			result = products.Find(item => (item as IProduct).ID == productId) as IProduct;
			return result != null;
		}

		public string[] ToList()
		{
			string currentPath = "";
			List<string> result = new List<string>();
			toList(currentPath, ref result);
			return result.ToArray();
		}

		private void toList(string currentPath, ref List<string> resultList)
		{
			currentPath = Path.Combine(currentPath, ID).Replace("\\", "/");
			resultList.Add(currentPath);
			foreach (Category subCategory in SubCategories) {
				subCategory.toList(currentPath, ref resultList);
			}
		}
	}
}
