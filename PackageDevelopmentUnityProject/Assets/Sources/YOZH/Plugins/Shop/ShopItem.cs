﻿using UnityEngine;
using YOZH.Plugins.UIExt;

namespace YOZH.Plugins.Shop
{
	public class ShopItem : ScriptableObject
	{
		public string ID { get { return name; } }

		[SerializeField]
		private UIPresentation uiPresentation = new UIPresentation();
		public UIPresentation UIPresentation { get { return uiPresentation; } }
	}
}