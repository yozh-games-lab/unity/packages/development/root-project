﻿using YOZH.Plugins.UIExt;

namespace YOZH.Plugins.Shop
{
	public interface IProduct
	{
		string ID { get; }
		string GoodID { get; }
		UIPresentation UIPresentation { get; }
		ExchangeRate[] Rates { get; }
		bool GetRate(string curencyId, out ExchangeRate rate);
	}
}