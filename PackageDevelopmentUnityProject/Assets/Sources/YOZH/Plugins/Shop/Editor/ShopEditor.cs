﻿using UnityEditor;
using YOZH.Core.Misc.Editor;
using System.IO;

namespace YOZH.Plugins.Shop.Editor
{
	[CustomEditor(typeof(Shop))]
	public class ShopEditor : UnityEditor.Editor
	{
		public static Shop CreateAsset()
		{
			return ScriptableObjectUtility.CreateScriptableObjectAtPath<Shop>(Path.Combine(Common.ASSETS_PATH, Shop.RESOURCE_PATH));
		}
	}
}