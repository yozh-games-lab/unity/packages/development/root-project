﻿using UnityEditor;
using YOZH.Core.GUIDLinks.Editor;

namespace YOZH.Plugins.Shop.Editor
{
	[CustomPropertyDrawer(typeof(ProductGUID), true)]
	public class ProductGUIDDrawer : GUIDLinkPropertyDrawer<IProduct>
	{
	}
}