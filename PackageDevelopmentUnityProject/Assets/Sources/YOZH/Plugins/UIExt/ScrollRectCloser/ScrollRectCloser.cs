﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace YOZH.Plugins.UIExt
{
	[RequireComponent(typeof(ScrollRect))]
	public class ScrollRectCloser : MonoBehaviour
	{
		[SerializeField]
		private float lerpSpeed = 10;

		[SerializeField]
		private float lerpEpsilonFactor = 0.1f;

		private ScrollRect thisScrollRect = null;

		private void Awake()
		{
			thisScrollRect = GetComponent<ScrollRect>();
		}

		[ContextMenu("NextStep")]
		public void NextStep()
		{
			StartCoroutine(doClosingTo(thisScrollRect.verticalNormalizedPosition, thisScrollRect.verticalNormalizedPosition + calculateStep(), Time.time));
		}

		[ContextMenu("PreviousStep")]
		public void PreviousStep()
		{
			StartCoroutine(doClosingTo(thisScrollRect.verticalNormalizedPosition, thisScrollRect.verticalNormalizedPosition - calculateStep(), Time.time));
		}

		//#region IEndDragHandler implementation
		//		public void OnEndDrag(PointerEventData eventData) {
		//			float step = calculateStep();
		//			float ceil = 1;
		//			float floor = 1 - step;
		//
		//			while (floor >= 0 && floor > thisScrollRect.verticalNormalizedPosition) {
		//				floor -= step;
		//				ceil -= step;
		//			}
		//
		//			float deltaToFloor = Mathf.Abs(thisScrollRect.verticalNormalizedPosition - floor);
		//			float deltaToCeil = Mathf.Abs(thisScrollRect.verticalNormalizedPosition - ceil);
		//
		//			if (deltaToFloor > deltaToCeil) {
		//				StartCoroutine(doClosingTo(thisScrollRect.verticalNormalizedPosition, ceil, Time.time));
		//			}
		//			else {
		//				StartCoroutine(doClosingTo(thisScrollRect.verticalNormalizedPosition, floor, Time.time));
		//			}
		//		}
		//#endregion

		private float calculateStep()
		{
			float stepCount = 0;
			for (int index = 0; index < thisScrollRect.content.childCount; index++) {
				if (thisScrollRect.content.GetChild(index).gameObject.activeSelf) {
					stepCount++;
				}
			}
			return (stepCount > 0) ? 1f / (stepCount) : 0;
		}

		private IEnumerator doClosingTo(float startValue, float targetValue, float startTime)
		{
			Debug.Log("from " + thisScrollRect.verticalNormalizedPosition);
			Debug.Log("to " + targetValue);

			while (Mathf.Abs(targetValue - thisScrollRect.verticalNormalizedPosition) > calculateStep() * lerpEpsilonFactor) {
				thisScrollRect.verticalNormalizedPosition = Mathf.Lerp(thisScrollRect.verticalNormalizedPosition, targetValue, Time.deltaTime * lerpSpeed);
				yield return new WaitForEndOfFrame();
			}
			thisScrollRect.verticalNormalizedPosition = targetValue;
		}
	}
}