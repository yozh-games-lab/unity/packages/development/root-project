﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.UIExt
{
	public class ProgressBarWithDelta : ProgressBar, IProgressBarWithDelta
	{
		private float delta = 0;
		public float Delta {
			get { return delta; }
			set {
				if (delta != value || Application.isEditor) {
					delta = normalizeDeltaValue(value);
					applyDeltaValueToTransform();
				}
			}
		}

		public override float Value {
			get { return base.Value; }
			set {
				base.Value = value;
				applyDeltaValueToTransform();
			}
		}

		[SerializeField]
		private RectTransform deltaTargetTransform = null;

		[SerializeField]
		private Text deltaLabel = null;
		public string DeltaLabelText {
			get { return (deltaLabel != null) ? deltaLabel.text : null; }
			set { if (deltaLabel != null) deltaLabel.text = value; }
		}

		protected override void Awake()
		{
			base.Awake();
			if (deltaTargetTransform == null && targetTransform.childCount > 0)
				deltaTargetTransform = (RectTransform)targetTransform.GetChild(0);
			applyDeltaValueToTransform();
		}

		private void setUpDeltaTransform()
		{
			deltaTargetTransform.parent = targetTransform;

			switch (direction) {
				case Direction.BottomToTop:
					deltaTargetTransform.anchorMin = new Vector2(0, 1);
					deltaTargetTransform.anchorMax = new Vector2(1, 1);
					break;
				case Direction.TopToBottom:
					deltaTargetTransform.anchorMin = new Vector2(0, 1);
					deltaTargetTransform.anchorMax = new Vector2(0, 0);
					break;
				case Direction.LeftToRight:
					deltaTargetTransform.anchorMin = new Vector2(1, 0);
					deltaTargetTransform.anchorMax = new Vector2(1, 1);
					break;
				case Direction.RightToLeft:
					deltaTargetTransform.anchorMin = new Vector2(0, 0);
					deltaTargetTransform.anchorMax = new Vector2(0, 1);
					break;
			}

			deltaTargetTransform.offsetMax = Vector2.zero;
			deltaTargetTransform.offsetMin = Vector2.zero;
		}

		private void applyDeltaValueToTransform()
		{
			float min;
			switch (direction) {
				case Direction.BottomToTop:
				case Direction.TopToBottom:
					min = targetTransform.anchorMax.y;
					deltaTargetTransform.anchorMin = new Vector2(0, min);
					deltaTargetTransform.anchorMax = new Vector2(1, min + delta);
					break;
				case Direction.LeftToRight:
				case Direction.RightToLeft:
					min = targetTransform.anchorMax.x;
					deltaTargetTransform.anchorMin = new Vector2(min, 0);
					deltaTargetTransform.anchorMax = new Vector2(min + delta, 1);
					break;
			}
		}

		private float normalizeDeltaValue(float delta)
		{
			if (delta > 1 - Value) delta = 1 - Value;
			delta = normalizeValue(delta);
			return delta;
		}
	}
}