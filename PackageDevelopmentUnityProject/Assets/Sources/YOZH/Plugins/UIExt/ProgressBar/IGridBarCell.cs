﻿using System;
using UnityEngine;

namespace YOZH.Plugins.UIExt
{
	public interface IGridBarCell : ICloneable
	{
		GridBarCellState State { get; set; }
		Component Instance { get; }
		void SetActive(bool active);
	}

	public enum GridBarCellState
	{
		None,
		Disabled,
		Enabled,
		ProgressDelta
	}

	/// <summary>
	/// Class-container to serialize an IGreedBarCell interface instance and to display it in the Unity Inspector
	/// </summary>
	[Serializable]
	public class SerializableIGridBarCell : IGridBarCell
	{
		[SerializeField]
		private GameObject cellGameObject;
		public GameObject CellGameObject {
			set {
				if (cellGameObject != value) {
					cellGameObject = value;
					cell = null;
				}
			}
		}

		private IGridBarCell cell = null;
		public IGridBarCell Cell {
			get {
				if (cell == null) {
					cell = cellGameObject.GetComponent<IGridBarCell>();
				}
				return cell;
			}
		}

		public GridBarCellState State {
			get { return Cell.State; }
			set { Cell.State = value; }
		}

		public Component Instance {
			get { return Cell.Instance; }
		}

		public object Clone()
		{
			return Cell.Clone();
		}

		public void SetActive(bool active)
		{
			Cell.SetActive(active);
		}
	}
}