﻿using System;
using UnityEngine;

namespace YOZH.Plugins.UIExt
{
	public interface IGridProgressBar : IProgressBarWithDelta
	{
		int CellCount { get; set; }
		IGridBarCell CellSample { get; set; }
	}

	/// <summary>
	/// Class-container to serialize an IGreedProgressBar interface instance and to display it in the Unity Inspector
	/// </summary>
	[Serializable]
	public class SerializableIGreedProgressBar : IGridProgressBar
	{
		[SerializeField]
		private GameObject cellGameObject;
		private IGridProgressBar bar = null;
		public IGridProgressBar Bar {
			get {
				if (bar == null) {
					bar = cellGameObject.GetComponent<IGridProgressBar>();
				}
				return bar;
			}
		}

		public int CellCount {
			get { return Bar.CellCount; }
			set { Bar.CellCount = value; }
		}

		public float Delta {
			get { return Bar.Delta; }
			set { Bar.Delta = value; }
		}

		public float Value {
			get { return Bar.Value; }
			set { Bar.Value = value; }
		}

		public IGridBarCell CellSample {
			get { return Bar.CellSample; }
			set { Bar.CellSample = value; }
		}

		public GameObject GameObject {
			get {
				return Bar.GameObject;
			}
		}

		public T FindComponent<T>() where T : Component
		{
			return Bar.FindComponent<T>();
		}
	}
}