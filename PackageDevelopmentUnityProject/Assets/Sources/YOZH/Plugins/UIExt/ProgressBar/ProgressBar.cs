﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.UIExt
{
	public class ProgressBar : MonoBehaviour, IProgressBar
	{
		private float value = 0;
		public virtual float Value {
			get { return value; }
			set {
				if (this.value != value || Application.isEditor) {
					this.value = normalizeValue(value);
					applyValueToTransform(this.value);
				}
			}
		}
		public string MinLabelText {
			get {
				return (minLabel != null) ? minLabel.text : null;
			}
			set {
				if (minLabel != null)
					minLabel.text = value;
			}
		}
		public string MaxLabelText {
			get {
				return (maxLabel != null) ? maxLabel.text : null;
			}
			set {
				if (maxLabel != null)
					maxLabel.text = value;
			}
		}
		public string ValueLabelText {
			get {
				return (valueLabel != null) ? valueLabel.text : null;
			}
			set {
				if (valueLabel != null)
					valueLabel.text = value;
			}
		}

		public GameObject GameObject {
			get {
				return gameObject;
			}
		}

		[SerializeField]
		protected Direction direction = Direction.LeftToRight;
		protected enum Direction { LeftToRight, RightToLeft, BottomToTop, TopToBottom }

		[SerializeField]
		protected RectTransform targetTransform = null;

		[SerializeField]
		private Text minLabel = null;
		[SerializeField]
		private Text maxLabel = null;
		[SerializeField]
		private Text valueLabel = null;

		protected virtual void Awake()
		{
			if (targetTransform == null) targetTransform = (RectTransform)transform;
			setUpTransform();
			applyValueToTransform(value);
		}

		protected virtual void setUpTransform()
		{
			targetTransform.anchorMin = Vector2.zero;
			targetTransform.anchorMax = Vector2.one;
			targetTransform.offsetMin = Vector2.zero;
			targetTransform.offsetMax = Vector2.zero;
		}

		private void applyValueToTransform(float val)
		{
			switch (direction) {
				case Direction.BottomToTop:
					targetTransform.anchorMax = new Vector2(1, val);
					break;
				case Direction.TopToBottom:
					targetTransform.anchorMin = new Vector2(0, 1 - val);
					break;
				case Direction.LeftToRight:
					targetTransform.anchorMax = new Vector2(val, 1);
					break;
				case Direction.RightToLeft:
					targetTransform.anchorMin = new Vector2(1 - val, 0);
					break;
			}
		}

		protected float normalizeValue(float val)
		{
			if (val > 1)
				val = 1;
			else if (val < 0)
				val = 0;
			return val;
		}

		public T FindComponent<T>() where T : Component
		{
			return GetComponent<T>();
		}
	}
}