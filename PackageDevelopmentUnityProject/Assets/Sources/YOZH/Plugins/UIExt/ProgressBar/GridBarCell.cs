﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.UIExt
{
	public class GridBarCell : MonoBehaviour, IGridBarCell
	{
		[SerializeField]
		protected Color disabledColor = Color.black;
		[SerializeField]
		protected Color progressDeltaColor = Color.grey;
		[SerializeField]
		protected Color enabledColor = Color.white;

		protected GridBarCellState state;
		public virtual GridBarCellState State {
			get {
				return state;
			}
			set {
				if (state != value) {
					state = value;
					switch (state) {
						case GridBarCellState.Enabled:
							setColor(enabledColor);
							break;

						case GridBarCellState.ProgressDelta:
							setColor(progressDeltaColor);
							break;

						case GridBarCellState.Disabled:
							setColor(disabledColor);
							break;
					}
				}
			}
		}

		public Component Instance {
			get { return this; }
		}

		public void SetActive(bool active)
		{
			gameObject.SetActive(active);
		}

		public object Clone()
		{
			GridBarCell clone = Instantiate(this);
			clone.transform.SetParent(transform.parent, false);
			clone.transform.SetAsLastSibling();
			return clone;
		}

		protected void setColor(Color color)
		{
			Graphic[] graphicsList = GetComponentsInChildren<Graphic>();
			foreach (Graphic graphic in graphicsList) {
				graphic.color = color;
			}
		}

		protected virtual void Awake()
		{
			State = GridBarCellState.Disabled;
		}
	}
}