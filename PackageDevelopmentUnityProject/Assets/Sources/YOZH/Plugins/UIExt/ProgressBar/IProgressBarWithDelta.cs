﻿namespace YOZH.Plugins.UIExt
{
	public interface IProgressBarWithDelta : IProgressBar
	{
		float Delta { get; set; }
	}
}