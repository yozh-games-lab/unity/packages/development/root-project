﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.WindowManager
{
	[RequireComponent(typeof(Button))]
	public class OpenChildWindowButton : MonoBehaviour
	{
		[SerializeField, WindowPath]
		private string winowPath = "";

		private void Awake()
		{
			GetComponent<Button>().onClick.AddListener(() =>
			{
				Window.SelectedWindow.OpenChild(winowPath);
			});
		}
	}
}