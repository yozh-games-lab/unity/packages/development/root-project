﻿using UnityEngine;
using UnityEditor;

namespace YOZH.Plugins.WindowManager.Editor
{
	[CustomEditor(typeof(Window), true), CanEditMultipleObjects]
	public class WindowEditor : UnityEditor.Editor
	{

		public override void OnInspectorGUI()
		{
			SerializedProperty awakeAsRoot = serializedObject.FindProperty("awakeAsRoot");
			SerializedProperty autoSelect = serializedObject.FindProperty("autoSelect");

			if (awakeAsRoot.boolValue) {
				EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"), true, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(awakeAsRoot, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(autoSelect, new GUILayoutOption[0]);
			}
			else {
				EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"), true, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(awakeAsRoot, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(autoSelect, new GUILayoutOption[0]);
				DrawPropertiesExcluding(serializedObject, "m_Script", "awakeAsRoot", "autoSelect");
			}
			serializedObject.ApplyModifiedProperties();
		}

	}
}