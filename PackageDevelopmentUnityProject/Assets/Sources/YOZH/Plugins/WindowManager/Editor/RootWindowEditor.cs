﻿using UnityEngine;
using UnityEditor;


namespace YOZH.Plugins.WindowManager.Editor
{
	[CustomEditor(typeof(RootWindow), true), CanEditMultipleObjects]
	public class RootWindowEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			SerializedProperty awakeAsRoot = serializedObject.FindProperty("awakeAsRoot");
			SerializedProperty autoSelect = serializedObject.FindProperty("autoSelect");
			SerializedProperty firstChildPath = serializedObject.FindProperty("firstChildPath");

			if (awakeAsRoot.boolValue) {
				EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"), true, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(awakeAsRoot, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(autoSelect, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(firstChildPath, new GUILayoutOption[0]);
			}
			else {
				EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"), true, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(awakeAsRoot, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(autoSelect, new GUILayoutOption[0]);
				EditorGUILayout.PropertyField(firstChildPath, new GUILayoutOption[0]);
				DrawPropertiesExcluding(serializedObject, "m_Script", "awakeAsRoot", "autoSelect", "firstChildPath");
			}
			serializedObject.ApplyModifiedProperties();
		}

	}
}