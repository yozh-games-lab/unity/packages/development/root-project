﻿using UnityEngine;


namespace YOZH.Plugins.WindowManager
{
	public class RootWindow : Window
	{
		[SerializeField, WindowPath]
		private string firstChildPath = "MainMenu/pref_window_MainMenu";

		protected override void onAwakeWindow()
		{
			base.onAwakeWindow();
			SelectRoot<Window>(WorkingCanvas).OpenChild(firstChildPath);
		}
	}
}