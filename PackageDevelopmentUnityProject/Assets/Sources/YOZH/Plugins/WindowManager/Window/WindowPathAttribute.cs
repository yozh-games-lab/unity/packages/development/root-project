﻿using YOZH.Core.CustomAttributes;
using System.Collections.Generic;

namespace YOZH.Plugins.WindowManager
{
	public class WindowPathAttribute : PrefabPathAttribute
	{
		public WindowPathAttribute() : base(typeof(Window))
		{
			List<string> filtered = new List<string>();
			foreach (string choice in Choices) {
				if (choice.StartsWith(Window.PATH_TO_WINDOWS_PREFABS)) {
					string correctChoice = choice.Remove(0, Window.PATH_TO_WINDOWS_PREFABS.Length).TrimStart(@"\/".ToCharArray());
					filtered.Add(correctChoice);
				}
			}
			Choices = filtered.ToArray();
		}

	}
}