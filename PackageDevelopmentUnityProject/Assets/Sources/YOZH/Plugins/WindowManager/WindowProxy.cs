﻿using UnityEngine;

namespace YOZH.Plugins.WindowManager
{
	public class WindowProxy : MonoBehaviour
	{
		public void AddRoot(Canvas canvas)
		{
			Window.AddRoot(canvas);
		}
		public void RemoveRoot(Canvas canvas)
		{
			Window.RemoveRoot(canvas);
		}
		public void SelectRoot(Canvas canvas)
		{
			Window.SelectRoot(canvas);
		}
		public void OpenRootChild(string path)
		{
			Window.CurrentRoot.OpenChild(path);
		}
		public void Close(Window win)
		{
			win.Close();
		}
		public void Select(Window win)
		{
			win.Select();
		}
		public void DestroyWin(Window win)
		{
			win.DestroyWin();
		}
	}
}