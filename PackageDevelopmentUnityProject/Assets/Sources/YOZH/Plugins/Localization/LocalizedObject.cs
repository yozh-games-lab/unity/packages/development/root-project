﻿using UnityEngine;

namespace YOZH.Plugins.Localization
{
	public abstract class LocalizedObject : MonoBehaviour
	{
		public bool UpdateContentOnAwake = true;
		public bool UpdateContentOnStart;
		public bool UpdateContentOnEnable;

		protected void Awake()
		{
			Localization.Instance.onLanguageChanged += UpdateContent;
			Init();
			if (UpdateContentOnAwake) {
				UpdateContent();
			}
		}

		protected void Start()
		{
			if (UpdateContentOnStart) {
				UpdateContent();
			}
		}

		protected void OnEnable()
		{
			if (UpdateContentOnEnable) {
				UpdateContent();
			}
		}

		protected void OnDestroy()
		{
			Localization.Instance.onLanguageChanged -= UpdateContent;
		}

		public abstract void UpdateContent();
		public abstract void Init();
	}
}