﻿using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.Localization
{
	public class Language : ScriptableObject
	{
		[SerializeField]
		private Font font = null;
		public Font Font {
			get {
				if (font == null)
					return Localization.Instance.DefaultFont;
				return font;
			}
		}

		[SerializeField]
		private TextSection[] textSections = { new TextSection(LocaleSectionNames.Common) };
		private Dictionary<string, object> textDictionary = new Dictionary<string, object>();

		public string GetText(string key)
		{
			object output;
			textDictionary.TryGetValue(key, out output);
			return (string)output;
		}

		public void Load(bool loadAll = true)
		{
			if (loadAll) {
				foreach (TextSection section in textSections) {
					section.Load(textDictionary);
				}
			}
			else {
				FindSection(textSections, LocaleSectionNames.Common).Load(textDictionary);
			}
		}

		public void Unload()
		{
			textDictionary.Clear();
		}

		public void LoadSection(string name)
		{
			FindSection(textSections, name).Load(textDictionary);
		}

		private LocaleSection FindSection(LocaleSection[] sections, string name)
		{
			for (int i = 0; i < sections.Length; i++) {
				if (sections[i].Name.ToString() == name) {
					return sections[i];
				}
			}
			return null;
		}

		private LocaleSection FindSection(LocaleSection[] sections, LocaleSectionNames name)
		{
			for (int i = 0; i < sections.Length; i++) {
				if (sections[i].Name == name) {
					return sections[i];
				}
			}
			return null;
		}
	}
}