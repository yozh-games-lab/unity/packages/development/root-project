﻿namespace YOZH.Plugins.Localization
{
	[System.Serializable]
	public class LocalizedString : LocalizedKeyData
	{
		private string value;
		public string Value {
			get {
				if (string.IsNullOrEmpty(value) || Localization.Instance.CurrentLanguage != language) {
					value = Localization.Instance.CurrentLanguage.GetText(key);
					language = Localization.Instance.CurrentLanguage;
				}
				return value ?? string.Format("[{0}]", Key);
			}
		}

		public LocalizedString(string key) : base(key) { }
		public LocalizedString(string key, string value) : base(key)
		{
			this.value = value;
		}

		public static explicit operator string(LocalizedString localizedString)
		{
			return localizedString.Value;
		}

		public override string ToString()
		{
			return Value;
		}
	}
}