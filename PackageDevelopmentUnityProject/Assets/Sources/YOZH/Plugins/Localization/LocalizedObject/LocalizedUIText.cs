﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.Localization
{
	[RequireComponent(typeof(Text))]
	public class LocalizedUiText : LocalizedObject
	{
		public string Key;
		private LocalizedString str;
		protected Text text;

		public void ChangeKey(string newKey)
		{
			Key = newKey;
			Init();
			UpdateContent();
		}

		public override void Init()
		{
			str = new LocalizedString(Key);
			text = this.gameObject.GetComponent<Text>();
		}

		public override void UpdateContent()
		{
			text.text = (string)str;
		}
	}
}