﻿using UnityEngine;
using System.Collections;
using System;
using YOZH.Core.CustomAttributes;

namespace YOZH.Plugin.Pooling.PoolV1
{
	[Serializable]
	public class PoolMeta
	{
		[SerializeField, PrefabPath(typeof(IPool))]
		private string path = "";
		public string Path { get { return path; } }

		[SerializeField, HideInInspector]
		private string poolName = "";
		public string PoolName { get { return poolName; } }

		[SerializeField, HideInInspector]
		private string sampleName = "";
		public string SampleName { get { return sampleName; } }

		[SerializeField]
		private bool preload = false;
		public bool Preload { get { return preload; } }

		private IPool cache = null;

		public PoolMeta(string path)
		{
			this.path = path;
		}

		public IPool Pool {
			get {
				if (cache == null) {
					GameObject prefab = Resources.Load<GameObject>(path);
					cache = prefab.GetComponent<IPool>();
				}
				return cache;
			}
		}

	}
}