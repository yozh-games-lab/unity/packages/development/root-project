﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

namespace YOZH.Plugin.Pooling.PoolV1
{
	public sealed class Pool : MonoBehaviour, IPool
	{
		[SerializeField]
		private GameObject sampleGameObject = null;
		[SerializeField]
		private int count = 0;
		[SerializeField]
		private bool fillInBackground = true;

		private List<IPoolable> pool = new List<IPoolable>();
		private Queue<IPoolable> queue = new Queue<IPoolable>();
		private IPoolable sample = null;
		private static readonly WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();

		[SerializeField]
		private bool autoIncrement = true;

		private void Awake()
		{
			sample = sampleGameObject.GetComponent<IPoolable>();
			if (fillInBackground) {
				StartCoroutine(fillRoutine());
			}
			else {
				fill();
			}
		}


		#region IPool implementation
		public string SampleName {
			get { return (sampleGameObject != null) ? sampleGameObject.name : ""; }
		}
		public void Purge()
		{
			foreach (IPoolable item in pool) {
				item.Dispose();
			}
			Resources.UnloadUnusedAssets();
			Destroy(gameObject);
		}
		public void Clear(bool soft = true)
		{

			IPoolable[] tmpList = pool.FindAll(item => !item.Busy).ToArray();

			foreach (IPoolable item in tmpList) {
				if (pool.Count == count) {
					break;
				}
				pool.Remove(item);
				item.Dispose();
			}

			if (!soft) {
				queue.Clear();
				tmpList = pool.ToArray();
				for (int i = tmpList.Length - 1; i >= count; i--) {
					pool.Remove(tmpList[i]);
					tmpList[i].Dispose();
				}
				foreach (IPoolable item in pool) {
					queue.Enqueue(item);
				}
			}

		}
		public IPoolable GetFromPool()
		{
			if (queue.Count == 0) {
				foreach (IPoolable item in pool) {
					if (!item.Busy) {
						queue.Enqueue(item);
					}
				}
				if (queue.Count == 0) {
					if (autoIncrement) {
						queue.Enqueue(cloneSample(sample));
					}
					else return null;
				}
			}
			IPoolable result = queue.Dequeue();
			result.PoolOut();
			return result;
		}

		public bool TryGetFromPool(out IPoolable result)
		{
			result = GetFromPool();
			return result != null;
		}
		public T FindComponent<T>() where T : Component
		{
			return GetComponent<T>();
		}

		public GameObject GameObject {
			get { return gameObject; }
		}
		#endregion

		private IEnumerator fillRoutine()
		{
			if (sample == null) yield break;

			for (int i = 0; i < count; i++) {
				queue.Enqueue(cloneSample(sample));
				yield return waitForEndOfFrame;
			}
		}
		private void fill()
		{
			if (sample == null) return;

			for (int i = 0; i < count; i++) {
				queue.Enqueue(cloneSample(sample));
			}
		}

		private IPoolable cloneSample(IPoolable sample)
		{
			if (sample == null) throw new ArgumentNullException();

			IPoolable clone = (IPoolable)sample.Clone();
			clone.OnPoolIn += (IPoolable sender) =>
			{
				sender.FindComponent<Transform>().SetParent(transform);
			};
			clone.OnPoolOut += (IPoolable sender) =>
			{
				sender.FindComponent<Transform>().SetParent(null);
			};

			clone.PoolIn();
			pool.Add(clone);
			return clone;
		}

	}
}
