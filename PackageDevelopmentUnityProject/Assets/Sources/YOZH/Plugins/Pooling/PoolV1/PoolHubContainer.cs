﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace YOZH.Plugin.Pooling.PoolV1
{
	public class PoolHubContainer : MonoBehaviour, IDictionary<string, IPool>
	{
		private Dictionary<string, IPool> cache = new Dictionary<string, IPool>();
		private Transform poolsRoot = null;
		public Transform PoolsRoot {
			get {
				if (poolsRoot == null) {
					poolsRoot = new GameObject("PoolsRoot").transform;
					poolsRoot.SetParent(transform);
				}
				return poolsRoot;
			}
		}
		private Transform itemsRoot = null;
		public Transform ItemsRoot {
			get {
				if (itemsRoot == null) {
					itemsRoot = new GameObject("ItemsRoot").transform;
					itemsRoot.SetParent(transform);
				}
				return itemsRoot;
			}
		}

		public void Clear(bool soft = true)
		{
			foreach (IPool pool in cache.Values) {
				pool.Clear(soft);
			}
		}

		#region IDictionary implementation
		public bool ContainsKey(string key)
		{
			return cache.ContainsKey(key);
		}
		public void Add(string key, IPool value)
		{
			cache.Add(key, value);
		}
		public bool Remove(string key)
		{
			return cache.Remove(key);
		}
		public bool TryGetValue(string key, out IPool value)
		{
			return cache.TryGetValue(key, out value);
		}
		public IPool this[string index] {
			get { return cache[index]; }
			set { cache[index] = value; }
		}
		public ICollection<string> Keys {
			get { return cache.Keys; }
		}
		public ICollection<IPool> Values {
			get { return cache.Values; }
		}
		#endregion
		#region ICollection implementation
		public void Add(KeyValuePair<string, IPool> item)
		{
			((ICollection<KeyValuePair<string, IPool>>)cache).Add(item);
		}
		public void Clear()
		{
			((ICollection<KeyValuePair<string, IPool>>)cache).Clear();
		}
		public bool Contains(KeyValuePair<string, IPool> item)
		{
			return ((ICollection<KeyValuePair<string, IPool>>)cache).Contains(item);
		}
		public void CopyTo(KeyValuePair<string, IPool>[] array, int arrayIndex)
		{
			((ICollection<KeyValuePair<string, IPool>>)cache).CopyTo(array, arrayIndex);
		}
		public bool Remove(KeyValuePair<string, IPool> item)
		{
			return ((ICollection<KeyValuePair<string, IPool>>)cache).Remove(item);
		}
		public int Count {
			get { return ((ICollection<KeyValuePair<string, IPool>>)cache).Count; }
		}
		public bool IsReadOnly {
			get { return ((ICollection<KeyValuePair<string, IPool>>)cache).IsReadOnly; }
		}
		#endregion
		#region IEnumerable implementation
		public IEnumerator<KeyValuePair<string, IPool>> GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<string, IPool>>)cache).GetEnumerator();
		}
		#endregion
		#region IEnumerable implementation
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<string, IPool>>)cache).GetEnumerator();
		}
		#endregion
	}
}