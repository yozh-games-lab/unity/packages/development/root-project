﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace YOZH.Plugin.Pooling.PoolV1.Editor
{
	[CustomPropertyDrawer(typeof(PoolMeta))]
	public class PoolMetaDrawer : PropertyDrawer
	{
		private const string PATH = "path";
		private const string POOL_NAME = "poolName";
		private const string SAMLPE_NAME = "sampleName";

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{

			SerializedProperty pathProperty = property.FindPropertyRelative(PATH);
			EditorGUI.PropertyField(position, property, label, true);

			GameObject prefab = Resources.Load<GameObject>(pathProperty.stringValue) as GameObject;
			if (prefab != null) {
				IPool pool = prefab.GetComponent<IPool>();
				if (pool != null) {
					property.FindPropertyRelative(POOL_NAME).stringValue = Path.GetFileNameWithoutExtension(pathProperty.stringValue);
					property.FindPropertyRelative(SAMLPE_NAME).stringValue = pool.SampleName;
				}
			}

		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUI.GetPropertyHeight(property);
		}
	}
}