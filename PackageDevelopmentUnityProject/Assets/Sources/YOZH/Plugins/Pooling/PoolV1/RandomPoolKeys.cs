﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using YOZH.Core.Misc;


namespace YOZH.Plugin.Pooling.PoolV1
{
	[Serializable]
	public class RandomPoolKeys
	{
		[Serializable]
		private class GetChance
		{
			[PoolKeyPropertyAttribute]
			public string Key = "";
			[Range(0, 100)]
			public int Chance = 100;
		}

		[SerializeField]
		private GetChance[] poolKeys = new GetChance[0];
		[SerializeField]
		private int minCount = 1;
		[SerializeField]
		private int maxCount = 1;

		public string[] GetKeys()
		{
			List<string> result = new List<string>();

			if (poolKeys.Length > 0) {
				float[] chances = new float[poolKeys.Length];
				for (int index = 0; index < chances.Length; index++) {
					chances[index] = poolKeys[index].Chance;
				}

				int count = Dice.Throw(minCount, maxCount);
				for (int counter = 0; counter < count; counter++) {

					int i = Dice.GetChoice(chances);
					string item = poolKeys[i].Key;
					if (!string.IsNullOrEmpty(item)) result.Add(item);
				}
			}

			return result.ToArray();
		}
	}
}