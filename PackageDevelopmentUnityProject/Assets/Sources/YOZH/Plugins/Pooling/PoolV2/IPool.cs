﻿namespace YOZH.Plugin.Pooling.PoolV2
{
	public interface IPool
	{
		void Prewarm(IPoolable sample);
		bool TryGet(IPoolable sample, out IPoolHandler result);
		void Return(IPoolHandler handler);
	}
}