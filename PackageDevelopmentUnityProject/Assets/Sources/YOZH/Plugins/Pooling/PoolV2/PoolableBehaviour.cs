﻿using System;
using UnityEngine;
using UnityEngine.Events;
using YOZH.Core.APIExtentions.Unity;

namespace YOZH.Plugin.Pooling.PoolV2
{
	internal class PoolableBehaviour : MonoBehaviour, IPoolable, IPoolHandler
	{
		[SerializeField] private UnityEvent OnPoolInEvent = null;
		[SerializeField] private UnityEvent OnPoolOutEvent = null;

		private bool IsDisposing = false;
		private IPool Pool;

		public string Name => name;
		private Component PoolComponent => Pool as Component;

		public event Action<IPoolable> OnPoolIn;
		public event Action<IPoolable> OnPoolOut;

		private void OnDestroy()
		{
			if (Pool != null && !IsDisposing && !ApplicationUtility.IsChangingPlaymode) {
#if UNITY_EDITOR
				Debug.LogAssertion(
					$"You are destroying {nameof(PoolableBehaviour)} wich is out of pool. It can affect pool's logic.");
#endif
			}
		}

		private void RaiseOnPoolIn()
		{
			OnPoolIn?.Invoke(this);
			OnPoolInEvent?.Invoke();
		}

		private void RaiseOnPoolOut()
		{
			OnPoolOut?.Invoke(this);
			OnPoolOutEvent?.Invoke();
		}

		IPoolable IPoolable.CreateInstance(IPool pool)
		{
			PoolableBehaviour result = Instantiate(this);
			result.Pool = pool;
			return result;
		}

		IPoolHandler IPoolable.AsHandler() => this as IPoolHandler;

		void IPoolable.PoolIn()
		{
			RaiseOnPoolIn();

			if (PoolComponent != null) {
				transform.SetParent(PoolComponent.transform);
				gameObject.SetActive(false);
			}
			else {
				Destroy(gameObject);
			}
		}

		void IPoolable.PoolOut()
		{
			if (PoolComponent != null) {
				transform.SetParent(null);
				gameObject.SetActive(true);
			}
			RaiseOnPoolOut();
		}

		T IPoolHandler.GetComponent<T>()
		{
			return GetComponent<T>();
		}

		void IDisposable.Dispose()
		{
			IsDisposing = true;
			Destroy(gameObject);
		}
	}
}