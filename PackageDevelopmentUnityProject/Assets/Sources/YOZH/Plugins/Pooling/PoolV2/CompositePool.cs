﻿using System.Collections.Generic;
using UnityEngine;
using YOZH.Core.CustomAttributes;

namespace YOZH.Plugin.Pooling.PoolV2
{
	internal class CompositePool : MonoBehaviour, IPool
	{
		[SerializeField, InterfaceField(typeof(IPool), allowSceneObject: true)] private Component SubpoolSample = null;

		private Dictionary<IPoolable, IPool> SamplePoolMap = new Dictionary<IPoolable, IPool>();
		private Dictionary<IPoolHandler, IPool> HandlerPoolMap = new Dictionary<IPoolHandler, IPool>();

		private IPool GetOrCreatePoolBySample(IPoolable sample)
		{
			IPool result;
			if (!SamplePoolMap.TryGetValue(sample, out result)) {
				result = Instantiate(SubpoolSample, Vector3.zero, Quaternion.identity, transform) as IPool;
				(result as Component).name = $"{sample.Name}_pool";
				SamplePoolMap.Add(sample, result);
			}

			return result;
		}

		void IPool.Prewarm(IPoolable sample)
		{
			GetOrCreatePoolBySample(sample).Prewarm(sample);
		}

		bool IPool.TryGet(IPoolable sample, out IPoolHandler result)
		{
			IPool pool = GetOrCreatePoolBySample(sample);

			if (pool.TryGet(sample, out result)) {
				HandlerPoolMap.Add(result, pool);
			}

			return result != null;
		}

		void IPool.Return(IPoolHandler handler)
		{
			IPool pool;
			if (HandlerPoolMap.TryGetValue(handler, out pool)) {
				HandlerPoolMap.Remove(handler);
				pool.Return(handler);
			}
		}
	}
}