﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YOZH.Core.APIExtentions.DotNET;

namespace YOZH.Plugin.Pooling.PoolV2
{
	internal class LazyPool : MonoBehaviour, IPool
	{
		[SerializeField] private int InitSize;
		[SerializeField] private int MaxSize = int.MaxValue;
		[SerializeField] private int PrewarmCountPerFrame = 1;

		private IPoolable Sample;

		private Queue<IPoolable> pooledIn = new Queue<IPoolable>();
		private Dictionary<IPoolHandler, IPoolable> pooledOut = new Dictionary<IPoolHandler, IPoolable>();
		private int Size => pooledIn.Count + pooledOut.Count;
		private bool IsFull => Size == MaxSize;
		private bool IsWarmedUp => Size >= InitSize;

		private IPoolable NewSampleInstance => !IsFull ? Sample.CreateInstance(this) : Sample.CreateInstance(null);

		public bool TryGet(IPoolable sample, out IPoolHandler result)
		{
			TrySetSample(sample);
			result = sample == Sample ? PoolOut()?.AsHandler() : null;
			return result != null;
		}

		public void Return(IPoolHandler handler)
		{
			IPoolable poolable;
			if (pooledOut.TryGetValue(handler, out poolable)) {
				pooledOut.Remove(handler);
				PoolIn(poolable);
			}
		}

		public void Prewarm(IPoolable sample)
		{
			if (sample != null) {
				TrySetSample(sample);
				StartCoroutine(PrewarmRoutine());
			}
		}

		private IEnumerator PrewarmRoutine()
		{
			while (Size < InitSize) {
				for (int count = 0; count < PrewarmCountPerFrame && Size < InitSize; count++) {
					PoolIn(NewSampleInstance);
				}
				yield return null;
			}
		}

		private void TrySetSample(IPoolable value)
		{
			if (value != null && Sample == null) {
				Sample = value;
			}
		}

		private void OnDestroy()
		{
			pooledIn.ForEach(item => item.Dispose());
		}

		private void OnValidate()
		{
			InitSize = Mathf.Max(InitSize, 0);
			MaxSize = Mathf.Max(InitSize, MaxSize);
			PrewarmCountPerFrame = Mathf.Clamp(PrewarmCountPerFrame, 1, InitSize);
		}

		private void PoolIn(IPoolable poolable)
		{
			pooledIn.Enqueue(poolable);
			poolable.PoolIn();
		}

		private IPoolable PoolOut()
		{
			if (!IsWarmedUp) {
				Prewarm(Sample);
			}

			IPoolable result = pooledIn.Count > 0 ? pooledIn.Dequeue() : NewSampleInstance;
			if (result != null) {
				result.PoolOut();
				pooledOut.Add(result.AsHandler(), result);
			}

			return result;
		}
	}
}