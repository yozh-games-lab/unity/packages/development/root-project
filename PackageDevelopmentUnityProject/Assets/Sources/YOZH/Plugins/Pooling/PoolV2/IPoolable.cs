﻿using System;

namespace YOZH.Plugin.Pooling.PoolV2
{
	public interface IPoolable : IDisposable
	{
		string Name { get; }

		IPoolable CreateInstance(IPool pool);
		IPoolHandler AsHandler();

		void PoolIn();
		void PoolOut();

		event Action<IPoolable> OnPoolIn;
		event Action<IPoolable> OnPoolOut;
	}
}