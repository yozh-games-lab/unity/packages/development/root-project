namespace YOZH.Plugin.Pooling.PoolV2
{
	public interface IPoolHandler
	{
		T GetComponent<T>();
	}
}