﻿using UnityEditor;
using UnityEngine;

namespace YOZH.Plugins.DevTools.DebugConsole.Editor
{
	public class AddConsoleToScene
	{
		[MenuItem("HBG/Dev tools/Add console")]
		private static void AddHBGConsole()
		{
			Console instance = GameObject.Instantiate(Resources.Load<Console>(Console.RESOURCE_PATH));
			instance.name = instance.name.Replace("(Clone)", "");
		}
	}
}