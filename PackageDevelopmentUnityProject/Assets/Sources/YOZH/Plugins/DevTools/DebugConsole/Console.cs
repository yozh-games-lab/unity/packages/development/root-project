﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

namespace YOZH.Plugins.DevTools.DebugConsole
{
	public class Console : MonoBehaviour
	{
		public const string RESOURCE_PATH = "Console";

		private static Console instance;
		public static Console Instance {
			get {
				if (instance == null) {
					Instance = Instantiate(Resources.Load<Console>(RESOURCE_PATH));
					Instance.name = Instance.name.Replace("(Clone)", "");
				}
				return instance;
			}
			set {
				if (instance == null && value != null) {
					instance = value;
					if (Application.isPlaying) DontDestroyOnLoad(instance);
				}
				else {
					DestroyImmediate(value);
				}
			}
		}

		[SerializeField]
		private Text title = null;
		[SerializeField]
		private bool autoScroll = true;
		[SerializeField]
		private LogView[] Logs = new LogView[1];

		[Serializable]
		private class LogView
		{
			public LogType Type = LogType.Log;
			public Color TextColor = Color.white;
			public bool ShowStack = false;
		}

		private Text uiText = null;
		private ScrollRect scroll = null;
		private List<string> messages = new List<string>();

		private void Awake()
		{
			Instance = this;
			if (title != null) {
				title.text = string.Format("{0} v{1}", Application.productName, Application.version);
			}

			scroll = GetComponentInChildren<ScrollRect>(true);
			setupUIText();

			Application.logMessageReceived += onLogReceived;
		}

		private void OnDestroy()
		{
			Application.logMessageReceived -= onLogReceived;
		}

		public void SwitchAutoscroll()
		{
			autoScroll = !autoScroll;
		}

		private void setupUIText()
		{
			uiText = scroll.GetComponentInChildren<Text>(true);
			uiText.supportRichText = true;
			uiText.text = "";
		}

		private void onLogReceived(string condition, string stackTrace, LogType type)
		{
			foreach (LogView logView in Logs) {
				if (logView.Type == type) {

					string message = string.Format("<color=#{0}>{1}</color>\n", ColorUtility.ToHtmlStringRGB(logView.TextColor), condition);
					if (logView.ShowStack) {
						message += string.Format("<size={0}><color=#{1}>------\n{2}------\n</color></size>", uiText.fontSize * 0.75f, ColorUtility.ToHtmlStringRGB(logView.TextColor), stackTrace);
					}
					messages.Add(message);

					string consoleText = "";
					int i;
					for (i = messages.Count - 1; i >= 0; i--) {
						if (consoleText.Length + messages[i].Length < 16000) {
							consoleText = messages[i] + consoleText;

						}
						else {
							messages.RemoveRange(0, i - 1);
							break;
						}
					}
					uiText.text = consoleText;
					if (autoScroll) {
						scroll.verticalScrollbar.value = 0;
					}
					return;
				}
			}
		}
	}
}