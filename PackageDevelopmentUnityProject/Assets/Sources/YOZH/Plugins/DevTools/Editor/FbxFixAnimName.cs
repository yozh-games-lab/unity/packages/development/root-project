﻿using UnityEditor;

namespace YOZH.Plugins.DevTools.Editor
{
	public class FbxFixAnimName : AssetPostprocessor
	{
		void OnPreprocessAnimation()
		{
			ModelImporter modelImporter = assetImporter as ModelImporter;

			ModelImporterClipAnimation[] clips = (modelImporter.clipAnimations.Length > 0) ? modelImporter.clipAnimations : modelImporter.defaultClipAnimations;
			foreach (ModelImporterClipAnimation clip in clips) {
				if (clip.name.Contains("|")) {
					string[] parts = clip.name.Split("|".ToCharArray());
					clip.name = parts[parts.Length - 1];
				}
			}
			modelImporter.clipAnimations = clips;
		}
	}
}