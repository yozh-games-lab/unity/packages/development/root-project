﻿namespace YOZH.Plugins.ProfileManager
{
	public interface IUserProfileDataReceiver
	{
		void ApplyUserProfileData(IUserProfileData data);
	}
}