﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using YOZH.Core.Misc;

namespace YOZH.Plugins.ProfileManager
{
	[Serializable]
	public class UserData : ISerializable
	{
		private List<KeyValue<string, object>> objValues = new List<KeyValue<string, object>>();

		public UserData()
		{
		}

		public void SetValue(string key, object value)
		{
			if (!value.GetType().IsSerializable) {
				throw new ArgumentException(string.Format("{0} is not serializable.", value.GetType()));
			}

			KeyValue<string, object> item = new KeyValue<string, object>(key, value);
			for (int i = 0; i < objValues.Count; i++) {
				if (String.CompareOrdinal(objValues[i].Key, key) == 0) {
					objValues[i] = item;
					return;
				}
			}
			objValues.Add(item);
		}

		public T GetValue<T>(string key, T defaultValue)
		{
			if (!typeof(T).IsSerializable) {
				throw new ArgumentException(string.Format("{0} is not serializable.", typeof(T)));
			}

			foreach (KeyValue<string, object> item in objValues) {
				if (String.CompareOrdinal(item.Key, key) == 0) {
					return (T)item.Value;
				}
			}
			return defaultValue;
		}

		#region ISerializable

		public UserData(SerializationInfo info, StreamingContext context)
		{
			objValues = info.GetValue("objValues", typeof(List<KeyValue<string, object>>)) as List<KeyValue<string, object>>;
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("objValues", objValues, typeof(List<KeyValue<string, object>>));
		}

		#endregion
	}
}