﻿namespace YOZH.Plugins.ProfileManager
{
	public interface IUserProfileDataProvider
	{
		IUserProfileData GetUserProfileData();
	}
}