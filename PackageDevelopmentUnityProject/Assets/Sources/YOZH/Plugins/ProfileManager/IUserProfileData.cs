﻿using System.Runtime.Serialization;

namespace YOZH.Plugins.ProfileManager
{
	public interface IUserProfileData : ISerializable
	{
		object RestoreObject();
	}
}