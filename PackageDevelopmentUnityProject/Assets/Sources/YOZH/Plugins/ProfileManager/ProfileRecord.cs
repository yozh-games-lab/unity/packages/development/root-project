﻿using System;
using System.IO;
using System.Runtime.Serialization;
using YOZH.Core.Misc;

namespace YOZH.Plugins.ProfileManager
{
	[Serializable]
	public class ProfileRecord : ISerializable
	{
		public string FileName { get; private set; }

		public string FileNameWithoutExtention {
			get { return Path.GetFileNameWithoutExtension(FileName); }
		}

		public const string FileExtention = "save";

		public int Id { get; private set; }
		public string UserName { get; private set; }
		public string UserUserDescription { get; private set; }
		public DateTime SaveDateTime { get; private set; }

		private string userData = null;

		public ProfileRecord(string userName)
		{
			UserName = userName;
		}

		public ProfileRecord(string userName, string userDescription) : this(userName)
		{
			UserUserDescription = userDescription;
		}

		public UserData GetUserSave(string userPassword)
		{
			UserData result = null;
			if (Serializer.TryConvertStringToObject<UserData>(userData, userPassword, out result)) {
				return result;
			}
			return null;
		}

		public void Delete(string directoryPath)
		{
			if (Directory.Exists(directoryPath) && !string.IsNullOrEmpty(FileName))
				File.Delete(Path.Combine(directoryPath, FileName));
		}

		public void SetUserSave(UserData userData, string userPassword)
		{
			this.userData = Serializer.ConvertObjectToString(userData, userPassword);
		}

		public static ProfileRecord Load(string directoryPath, string fileName)
		{
			ProfileRecord result = null;
			if (Serializer.TryLoad(Path.Combine(directoryPath, fileName), out result)) {
				result.FileName = fileName;
				return result;
			}
			return null;
		}

		public void SaveRecord(string directoryPath)
		{
			if (string.IsNullOrEmpty(FileName))
				FileName = generateRecordFileName(directoryPath);

			save(directoryPath);
		}

		private void save(string directoryPath)
		{
			SaveDateTime = DateTime.Now;
			Serializer.Save(this, Path.Combine(directoryPath, FileName));
		}

		private string generateRecordFileName(string directoryPath)
		{
			string[] files = Directory.GetFiles(directoryPath);
			Id = getFilesLastId(directoryPath, files) + 1;

			return string.Format("{0}_{1}_data.{2}", UserName, Id, FileExtention);
		}

		private int getFilesLastId(string directoryPath, string[] files)
		{
			int maxId = 0;

			for (int index = 0; index < files.Length; index++) {
				var fileName = Path.GetFileName(files[index]);
				if (!string.IsNullOrEmpty(fileName)) {
					ProfileRecord profileRecord = Load(directoryPath, fileName);
					if (profileRecord != null && profileRecord.UserName == UserName)
						if (maxId < profileRecord.Id)
							maxId = profileRecord.Id;
				}
			}
			return maxId;
		}

		#region ISerializable

		public ProfileRecord(SerializationInfo info, StreamingContext context)
		{
			Id = info.GetInt32("Id");
			UserName = info.GetString("UserName");
			UserUserDescription = info.GetString("UserUserDescription");
			SaveDateTime = info.GetDateTime("SaveDateTime");
			userData = info.GetString("ProfileData");
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("Id", Id);
			info.AddValue("UserName", UserName);
			info.AddValue("UserUserDescription", UserUserDescription);
			info.AddValue("SaveDateTime", SaveDateTime);
			info.AddValue("ProfileData", userData);
		}

		#endregion

		public override string ToString()
		{
			return string.Format("[{0}] {1}: {2}", Id, FileName, UserName);
		}
	}
}