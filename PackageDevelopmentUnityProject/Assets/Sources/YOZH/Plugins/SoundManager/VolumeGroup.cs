using System;
using UnityEngine;

namespace YOZH.Plugins.SoundManager
{
	[Serializable]
	public class VolumeGroup
	{
		public string Name = typeof(VolumeGroup).ToString();
		private bool storeVolumeInPlayerPrefs {
			get {
				if (AudioLibrary.Instance != null) {
					return AudioLibrary.Instance.StoreVolumeInPlayerPrefs;
				}
				return false;
			}
		}

		private float volume = -1;
		public float Volume {
			get {
				if (volume < 0) {
					if (storeVolumeInPlayerPrefs)
						volume = PlayerPrefs.GetFloat(string.Format("{0}.{1}.Volume", typeof(VolumeGroup), Name), 0.5f);
				}
				return volume;
			}
			set {
				float v = volume;
				if (value >= 0 && value <= 1) volume = value;
				else
					if (value < 0) volume = 0;
				else
					if (value > 1) volume = 1;

				if (volume != v) {
					_riseOnVolumeChanged();
					if (storeVolumeInPlayerPrefs) {
						PlayerPrefs.SetFloat(string.Format("{0}.{1}.Volume", typeof(VolumeGroup), Name), volume);
					}
				}
			}
		}

		public VolumeGroup(VolumeGroupInfo groupInfo)
		{
			Name = groupInfo.Name;
			if (Volume < 0) Volume = groupInfo.Volume;
		}

		private EventHandler onVolumeChanged;
		public event EventHandler OnVolumeChanged {
			add {
				bool allowAssignment = true;
				if (onVolumeChanged != null) {
					Delegate[] delegates = onVolumeChanged.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) {
						if (delegates[i] == (Delegate)value) {
							allowAssignment = false;
							break;
						}
					}
				}
				if (allowAssignment) onVolumeChanged += value;
			}
			remove { onVolumeChanged -= value; }
		}
		private void _riseOnVolumeChanged()
		{
			if (onVolumeChanged != null)
				onVolumeChanged(this, new EventArgs());
		}
	}
}