using UnityEngine;

namespace YOZH.Plugins.SoundManager
{
	public class TestSoundManager : MonoBehaviour
	{
		[Range(0, 1)]
		public float TotalVol = 0.5f;

		[Range(0, 1)]
		public float GroupVol = 0.5f;
		public string VolGroupName = typeof(VolumeGroup).ToString();

		public string SoundName;
		public float TimeScale = 1;

		void Start()
		{
			InvokeRepeating("Play", 3, 3);
		}

		void Update()
		{
			SoundManager.Instance.Mixer.Volume = TotalVol;
			SoundManager.Instance.Mixer.SetVolume(VolGroupName, GroupVol);
			Time.timeScale = TimeScale;
		}

		void Play()
		{
			SoundManager.Instance.PlaySound(SoundName);
		}
	}
}