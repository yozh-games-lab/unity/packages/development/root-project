﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.SoundManager
{
	[RequireComponent(typeof(Toggle))]
	public class VolumeToggle : MonoBehaviour
	{
		[SerializeField]
		private bool inverseOnOff = false;

		[SerializeField]
		private string volumeGroup = "";
		[SerializeField]
		private Toggle toggle = null;
		private Toggle Toggle {
			get {
				if (toggle == null)
					toggle = GetComponent<Toggle>();
				return toggle;
			}
		}

		void Awake()
		{
			Toggle.onValueChanged.AddListener(OnValueChanged);

			if (inverseOnOff)
				Toggle.isOn = !Convert.ToBoolean(SoundManager.Instance.Mixer.GetVolume(volumeGroup));
			else
				Toggle.isOn = Convert.ToBoolean(SoundManager.Instance.Mixer.GetVolume(volumeGroup));
		}

		public void OnValueChanged(bool isOn)
		{
			if (inverseOnOff)
				SoundManager.Instance.Mixer.SetVolume(volumeGroup, Convert.ToInt16(!isOn));
			else
				SoundManager.Instance.Mixer.SetVolume(volumeGroup, Convert.ToInt16(isOn));
		}
	}
}