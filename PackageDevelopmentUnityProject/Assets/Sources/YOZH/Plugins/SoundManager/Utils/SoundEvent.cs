﻿using UnityEngine;
using System.Collections;
using System;


namespace YOZH.Plugins.SoundManager
{
	[Serializable]
	public class SoundEvent
	{
		[SoundName]
		public string sound = "";
		public bool enabled = false;

		private Sound playingSound = null;

		public bool IsFinished()
		{
			return (!enabled || playingSound == null);
		}

		public void Execute()
		{
			if (!enabled) return;

			playingSound = SoundManager.Instance.PlaySound(sound);
			if (playingSound != null) {
				playingSound.OnStop += (Sound snd) =>
				{
					playingSound = null;
				};
			}
		}

		public void Break()
		{
			if (!enabled) return;

			if (playingSound != null) {
				playingSound.Stop();
			}
		}
	}
}