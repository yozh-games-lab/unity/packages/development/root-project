﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.SoundManager
{
	[RequireComponent(typeof(Slider))]
	public class VolumeSlider : MonoBehaviour
	{
		private Slider _slider;
		[SerializeField, HideInInspector]
		private string volumeGroupName = "";

		void Awake()
		{
			_slider = GetComponent<Slider>();
			if (_slider != null) {
				_slider.maxValue = 1;
				_slider.minValue = 0;
				_slider.value = SoundManager.Instance.Mixer.GetVolume(volumeGroupName);
				_slider.onValueChanged.AddListener(_onValueChanged);
			}
		}

		private void _onValueChanged(float val)
		{
			SoundManager.Instance.Mixer.SetVolume(volumeGroupName, val);
		}
	}
}