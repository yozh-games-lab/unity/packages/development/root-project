using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;


namespace YOZH.Plugins.SoundManager
{
	public delegate void SoundDelegate(Sound sound);

	[RequireComponent(typeof(AudioSource))]
	public class Sound : MonoBehaviour
	{
		#region Variables
		public bool Reserved = false;
		public string poolName { get; private set; }

		private AudioSource sourceHolder = null;
		private AudioSource source {
			get {
				if (sourceHolder == null) sourceHolder = GetComponent<AudioSource>();
				return sourceHolder;
			}
		}

		public AudioClip clip {
			get { return source.clip; }
			set {
				if (source.clip != value) {
					source.clip = value;
				}
			}
		}

		public bool PauseOnTimeHold = false;
		public bool TimeDependent = false;

		public bool IsPlaying { get { return source.isPlaying; } }

		private float volume = -1;
		public float Volume {
			get { return volume; }
			set {
				float v = volume;
				if (value >= 0 && value <= 1) volume = value;
				else if (value < 0) volume = 0;
				else if (value > 1) volume = 1;

				if (volume != v) {
					updateGroupedVolume();
					riseOnVolumeChanged();
				}
			}
		}

		private bool isPaused = false;
		public bool IsPaused { get { return isPaused; } }

		private float startTimeHolder = -1;
		private float startTime {
			get {

				if (startTimeHolder >= 0 && startTimeHolder < clip.length) {
					return startTimeHolder;
				}
				return 0;
			}
			set {
				startTimeHolder = value;
			}
		}

		private float endTimeHolder = -1;
		private float endTime {
			get {

				if (endTimeHolder > 0 && endTimeHolder <= clip.length) {
					return endTimeHolder;
				}
				return clip.length;
			}
			set { endTimeHolder = value; }
		}

		private float pitch {
			get { return source.pitch; }
			set { source.pitch = value; }
		}
		public float Pitch {
			get { return pitch; }
			set {
				if (!TimeDependent) {
					pitch = value;
				}
			}
		}

		public AudioMixerGroup OutputMixer {
			get { return source.outputAudioMixerGroup; }
			set { source.outputAudioMixerGroup = value; }
		}

		public bool Looped {
			get { return source.loop; }
			set { source.loop = value; }
		}

		public float TimeToEnd {
			get {
				if (IsPlaying) {
					return endTime - source.time;
				}
				return endTime - startTime;
			}
		}

		public float Length { get { return endTime - startTime; } }
		public float Time { get { return Length - TimeToEnd; } }

		public int priority {
			get { return source.priority; }
			set { source.priority = value; }
		}

		private SoundInfo defaultSetup = new SoundInfo();
		private VolumeGroup volumeGroup = null;
		private IEnumerator playRoutineContainer = null;
		#endregion

		#region Events
		private SoundDelegate onDestroyEvent;
		public event SoundDelegate OnDestroyEvent {
			add {
				if (onDestroyEvent != null) {
					Delegate[] delegates = onDestroyEvent.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) if (delegates[i] == (Delegate)value) return;
				}
				onDestroyEvent += value;
			}
			remove {
				onDestroyEvent -= value;
			}
		}
		protected void riseOnDestroy()
		{
			if (onDestroyEvent != null) onDestroyEvent(this);
		}

		private SoundDelegate onVolumeChanged;
		public event SoundDelegate OnVolumeChanged {
			add {
				if (onVolumeChanged != null) {
					Delegate[] delegates = onVolumeChanged.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) if (delegates[i] == (Delegate)value) return;
				}
				onVolumeChanged += value;
			}
			remove {
				onVolumeChanged -= value;
			}
		}
		protected void riseOnVolumeChanged()
		{
			if (onVolumeChanged != null) onVolumeChanged(this);
		}

		private SoundDelegate onPlay;
		public event SoundDelegate OnPlay {
			add {
				if (onPlay != null) {
					Delegate[] delegates = onPlay.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) if (delegates[i] == (Delegate)value) return;
				}
				onPlay += value;
			}
			remove {
				onPlay -= value;
			}
		}
		protected void riseOnPlay()
		{
			if (onPlay != null) onPlay(this);
		}

		private SoundDelegate onStop;
		public event SoundDelegate OnStop {
			add {
				if (onStop != null) {
					Delegate[] delegates = onStop.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) if (delegates[i] == (Delegate)value) return;
				}
				onStop += value;
			}
			remove { onStop -= value; }
		}
		protected void riseOnStop()
		{
			if (onStop != null) onStop(this);
		}

		private SoundDelegate onPause;
		public event SoundDelegate OnPause {
			add {
				if (onPause != null) {
					Delegate[] delegates = onPause.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) if (delegates[i] == (Delegate)value) return;
				}
				onPause += value;
			}
			remove { onPause -= value; }
		}
		protected void riseOnPause()
		{
			if (onPause != null) onPause(this);
		}

		private SoundDelegate onHold;
		public event SoundDelegate OnHold {
			add {
				if (onHold != null) {
					Delegate[] delegates = onHold.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) if (delegates[i] == (Delegate)value) return;
				}
				onHold += value;
			}
			remove { onHold -= value; }
		}
		protected void riseOnHold()
		{
			if (onHold != null) onHold(this);
		}

		private SoundDelegate onUnhold;
		public event SoundDelegate OnUnhold {
			add {
				if (onUnhold != null) {
					Delegate[] delegates = onUnhold.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) if (delegates[i] == (Delegate)value) return;
				}
				onUnhold += value;

			}
			remove { onUnhold -= value; }
		}
		protected void riseOnUohold()
		{
			if (onUnhold != null) onUnhold(this);
		}

		private void Awake()
		{
			cacheAudioSource();

			SoundManager.OnTimeScaleChanged += onTimeScaleChanged;
			setup(defaultSetup);
		}

		private void OnDestroy()
		{
			riseOnDestroy();

			SoundManager.OnTimeScaleChanged -= onTimeScaleChanged;
			SoundManager.UnregisterVolumeCallback(volumeGroup, onSoundManagerVolumeChanged);
		}
		#endregion

		#region PublicFuncs
		public void Play()
		{
			play();
		}

		public void Pause()
		{
			pause();
		}

		public void Stop()
		{
			stop();
		}

		public static Sound Create(SoundInfo track)
		{
			if (track == null) return null;

			Sound sound = CreateEmpty();
			sound.setup(track);
			return sound;
		}

		public static Sound CreateEmpty()
		{
			GameObject go = new GameObject(typeof(Sound).ToString());
			Sound sound = go.AddComponent<Sound>();
			return sound;
		}

		public void Setup(SoundInfo track)
		{
			clearEvents();
			setup(track);
		}
		#endregion

		#region PrivateFuncs
		private void onSoundManagerVolumeChanged(object sender, EventArgs args)
		{
			updateGroupedVolume();
		}

		private void onTimeScaleChanged(object sender, EventArgs args)
		{
			applyTimeScale();
		}

		private void cacheAudioSource()
		{
			source.playOnAwake = false;
			source.Stop();
		}

		private void updateGroupedVolume()
		{
			if (volumeGroup != null) source.volume = volume * volumeGroup.Volume * SoundManager.Instance.Mixer.Volume;
			else source.volume = volume;
		}

		private void applyTimeScale()
		{
			if (!TimeDependent) return;
			if (PauseOnTimeHold) {
				if (UnityEngine.Time.timeScale == 0) {
					source.Pause();
					riseOnHold();
				}
				else {
					source.Play();
					riseOnUohold();
				}
			}
			pitch = UnityEngine.Time.timeScale;
		}

		private IEnumerator playRoutine(float timeStart, float timeEnd)
		{

			while (IsPlaying && (Looped || source.time <= timeEnd)) {
				if (Looped && source.time >= timeEnd) source.time = timeStart;
				yield return new WaitForEndOfFrame();
			}
			stop();
		}

		private void play()
		{
			if (IsPlaying) return;
			source.time = 0;
			source.Play();
			if (!isPaused) {
				source.time = startTime;
				startPlayRoutine();
			}
			riseOnPlay();
			isPaused = false;
		}

		private void stop()
		{
			stopPlayRoutine();
			isPaused = false;
			source.Stop();
			riseOnStop();
		}

		private void pause()
		{
			isPaused = true;
			source.Pause();
			riseOnPause();
		}

		private void startPlayRoutine()
		{
			stopPlayRoutine();
			playRoutineContainer = playRoutine(startTime, endTime);
			StartCoroutine(playRoutineContainer);
		}

		private void stopPlayRoutine()
		{
			if (playRoutineContainer != null) {
				StopCoroutine(playRoutineContainer);
				playRoutineContainer = null;
			}
		}

		private void setup(SoundInfo track)
		{
			if (track == null) return;

			gameObject.name = track.SoundName;
			priority = track.Priority;
			poolName = track.PoolName;
			clip = track.Clip;
			Volume = track.Volume;
			Looped = track.Looped;
			startTime = track.StartTime;
			endTime = track.EndTime;
			PauseOnTimeHold = track.PauseOnTimeHold;
			TimeDependent = track.TimeDependent;
			Pitch = track.Pitch;
			OutputMixer = track.OutputMixer;
			volumeGroup = SoundManager.Instance.Mixer.GetVolumeGroup(track.VolumeGroup);
			SoundManager.RegisterVolumeCallback(volumeGroup, onSoundManagerVolumeChanged);
			updateGroupedVolume();
			applyTimeScale();
		}

		private void clearEvents()
		{
			SoundManager.UnregisterVolumeCallback(volumeGroup, onSoundManagerVolumeChanged);
			onHold = null;
			onUnhold = null;
			onPlay = null;
			onPause = null;
			onStop = null;
			onVolumeChanged = null;
		}
		#endregion
	}
}