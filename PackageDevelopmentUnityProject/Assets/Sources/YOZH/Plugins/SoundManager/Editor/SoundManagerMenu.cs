using UnityEditor;
using YOZH.Core.Misc.Editor;

namespace YOZH.Plugins.SoundManager.Editor
{
	public class SoundManagerMenu : UnityEditor.Editor
	{
		[MenuItem("HBG/Sound manager/Create audio library")]
		[MenuItem("Assets/Create/New audio library")]
		public static void NewAudioLibrary()
		{
			ScriptableObjectUtility.CreateScriptableObjectAtPath<AudioLibrary>(
				string.Format("HBG/Resources/{0}/{1}", SoundManager.RESOURCE_PATH, AudioLibrary.RESOURCE_PATH), true);
		}
	}
}