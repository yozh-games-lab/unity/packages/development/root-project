﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace YOZH.Plugins.SoundManager.Editor
{
	[CustomPropertyDrawer(typeof(SoundInfo))]
	public class SoundInfoEditor : PropertyDrawer
	{
		private static Sound testSound;

		private static bool isPlayingTestSound {
			get {
				return (testSound != null && testSound.IsPlaying);
			}
		}

		private float margin = 0;
		private float lineHeight = EditorGUIUtility.singleLineHeight;
		private float lineInterval = EditorGUIUtility.standardVerticalSpacing;

		private static void playClip(AudioClip clip, float startTime, bool loop)
		{
			int startSample = (int)(startTime * clip.frequency);

			Assembly assembly = typeof(AudioImporter).Assembly;
			Type audioUtilType = assembly.GetType("UnityEditor.AudioUtil");

			Type[] typeParams = { typeof(AudioClip), typeof(int), typeof(bool) };
			object[] objParams = { clip, startSample, loop };

			MethodInfo method = audioUtilType.GetMethod("PlayClip", typeParams);
			method.Invoke(null, BindingFlags.Static | BindingFlags.Public, null, objParams, null);
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);
			EditorGUI.indentLevel++;

			GUI.skin.label.alignment = TextAnchor.MiddleLeft;
			GUI.skin.textField.alignment = TextAnchor.MiddleLeft;

			SerializedProperty soundName = property.FindPropertyRelative("SoundName");
			Rect currentLine = getFirstLine(position);
			float foldoutSize = 30;

			soundName.isExpanded = EditorGUI.Foldout(new Rect(currentLine.x, currentLine.y, foldoutSize, currentLine.height), soundName.isExpanded, GUIContent.none);
			EditorGUI.indentLevel--;

			EditorGUI.PropertyField(
				new Rect(currentLine.x + foldoutSize,
					currentLine.y,
					currentLine.width - ((Application.isPlaying) ? 2 * foldoutSize + lineInterval : foldoutSize),
					currentLine.height),
				soundName,
				new GUIContent(soundName.displayName));

			if (Application.isPlaying) {
				if (isPlayingTestSound && soundName.stringValue == testSound.name) {
					if (GUI.Button(new Rect(currentLine.x + currentLine.width - foldoutSize, currentLine.y, foldoutSize, currentLine.height), "‖")) {
						testSound.Stop();
					}
				}
				else {
					if (GUI.Button(new Rect(currentLine.x + currentLine.width - foldoutSize, currentLine.y, foldoutSize, currentLine.height), "►")) {
						if (isPlayingTestSound) testSound.Stop();

						testSound = SoundManager.Instance.PlaySound(soundName.stringValue);
					}
				}
			}

			EditorGUI.indentLevel++;
			currentLine = getNextLine(currentLine);
			if (soundName.isExpanded) {
				EditorGUI.indentLevel += 2;

				SerializedProperty clipProp = property.FindPropertyRelative("Clip");
				currentLine = drawDefaultField(clipProp, currentLine);

				SerializedProperty startTime = property.FindPropertyRelative("StartTime");
				SerializedProperty endTime = property.FindPropertyRelative("EndTime");

				if (clipProp.objectReferenceValue != null) {
					EditorGUI.LabelField(currentLine, "Sound Length:");
					currentLine = getNextLine(currentLine);

					AudioClip clip = (AudioClip)clipProp.objectReferenceValue;

					float min = (startTime.floatValue >= 0 && startTime.floatValue < clip.length) ? startTime.floatValue : 0;
					float max = (endTime.floatValue >= 0 && endTime.floatValue <= clip.length) ? endTime.floatValue : clip.length;

					float textWidth = 120;
					Rect minRect = new Rect(currentLine.x, currentLine.y, textWidth, currentLine.height);
					Rect maxRect = new Rect(currentLine.x + currentLine.width - textWidth, currentLine.y, textWidth, currentLine.height);
					Rect sliderRect = new Rect(currentLine.x + textWidth, currentLine.y, currentLine.width - textWidth * 2, currentLine.height);

					min = EditorGUI.FloatField(minRect, min);
					max = EditorGUI.FloatField(maxRect, max);
					EditorGUI.MinMaxSlider(sliderRect, ref min, ref max, 0, clip.length);

					startTime.floatValue = min;
					endTime.floatValue = max;
					currentLine = getNextLine(currentLine);
				}
				else {
					startTime.floatValue = -1;
					endTime.floatValue = -1;
				}

				SerializedProperty priorityProp = property.FindPropertyRelative("Priority");
				currentLine = drawDefaultField(priorityProp, currentLine);

				if (AudioLibrary.Instance != null) {
					SerializedProperty poolName = property.FindPropertyRelative("PoolName");
					drawPopup(poolName, AudioLibrary.Instance.GetPoolsNames(), currentLine);
					currentLine = getNextLine(currentLine);

					SerializedProperty volumeGroup = property.FindPropertyRelative("VolumeGroup");
					drawPopup(volumeGroup, AudioLibrary.Instance.GetVolumeGroupsNames(), currentLine);
					currentLine = getNextLine(currentLine);
				}
				else {
					currentLine = new Rect(currentLine.x, currentLine.y, currentLine.width, currentLine.height * 2 + lineInterval);
					GUI.enabled = false;
					EditorGUI.TextField(currentLine,
						"Please create a new audio library.\nThen you will be able to choose a pool and Volume group for this sound.");
					GUI.enabled = true;
					currentLine = getNextLine(currentLine);
				}
				currentLine = drawDefaultField(property.FindPropertyRelative("Volume"), currentLine);
				currentLine = drawDefaultField(property.FindPropertyRelative("Looped"), currentLine);
				currentLine = drawDefaultField(property.FindPropertyRelative("PauseOnTimeHold"), currentLine);
				currentLine = drawDefaultField(property.FindPropertyRelative("TimeDependent"), currentLine);
				currentLine = drawDefaultField(property.FindPropertyRelative("Pitch"), currentLine);
				currentLine = drawDefaultField(property.FindPropertyRelative("OutputMixer"), currentLine);
				EditorGUI.indentLevel -= 2;
			}
			EditorGUI.indentLevel--;
			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
		{
			int linesCount = 1;

			SerializedProperty soundName = prop.FindPropertyRelative("SoundName");
			if (soundName.isExpanded) {
				linesCount = 13;
				if (AudioLibrary.Instance == null) {
					linesCount -= 0;
				}

				SerializedProperty clipProp = prop.FindPropertyRelative("Clip");
				if (clipProp.objectReferenceValue == null) {
					linesCount -= 2;
				}
			}
			return (lineHeight + lineInterval) * linesCount - lineInterval + margin;
		}

		private Rect getNextLine(Rect previousLine)
		{
			return new Rect(previousLine.x, previousLine.yMax + lineInterval, previousLine.width, lineHeight);
		}

		private Rect getFirstLine(Rect propPos)
		{
			return new Rect(propPos.x, propPos.y + margin, propPos.width, lineHeight);
		}

		private void drawPopup(SerializedProperty prop, string[] items, Rect pos)
		{
			int selectedIndex = 0;
			for (selectedIndex = items.Length - 1; selectedIndex > 0; selectedIndex--) {
				if (prop.stringValue == items[selectedIndex]) break;
			}
			prop.stringValue = items[EditorGUI.Popup(pos, prop.displayName, selectedIndex, items)];
		}

		private Rect drawDefaultField(SerializedProperty prop, Rect rect)
		{
			EditorGUI.PropertyField(rect, prop, new GUIContent(prop.displayName));
			return getNextLine(rect);
		}
	}
}