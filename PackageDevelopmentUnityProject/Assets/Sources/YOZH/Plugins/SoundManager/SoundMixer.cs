using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace YOZH.Plugins.SoundManager
{
	public class SoundMixer : MonoBehaviour
	{
		#region Variables
		private Dictionary<string, VolumeGroup> volumeGroups = new Dictionary<string, VolumeGroup>();
		private Dictionary<Sound, IEnumerator> fadeRoutines = new Dictionary<Sound, IEnumerator>();
		#endregion

		#region Properties
		private const float DEFAULT_VOLUME = 1;

		private bool storeVolumeInPlayerPrefs {
			get {
				if (AudioLibrary.Instance != null) {
					return AudioLibrary.Instance.StoreVolumeInPlayerPrefs;
				}
				return false;
			}
		}

		private float volume = -1;
		public float Volume {
			get {
				if (volume < 0) {
					if (storeVolumeInPlayerPrefs) volume = PlayerPrefs.GetFloat(string.Format("{0}.Volume", typeof(SoundMixer)),
																				   DEFAULT_VOLUME);
					else volume = DEFAULT_VOLUME;
				}
				return volume;
			}
			set {
				float v = volume;
				if (value >= 0 && value <= 1) volume = value;
				else if (value < 0) volume = 0;
				else if (value > 1) volume = 1;
				if (volume != v) {
					if (storeVolumeInPlayerPrefs) {
						PlayerPrefs.SetFloat(string.Format("{0}.Volume", typeof(SoundMixer)), volume);
					}
					riseOnTotalVolumeChanged();
				}
			}
		}
		#endregion

		#region Events
		private void Awake()
		{
			fillVolumeGroups();
		}

		private EventHandler onTotalVolumeChanged;
		public event EventHandler OnTotalVolumeChanged {
			add {
				if (onTotalVolumeChanged != null) {
					Delegate[] delegates = onTotalVolumeChanged.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) if (delegates[i] == (Delegate)value) return;
				}
				onTotalVolumeChanged += value;
			}
			remove { onTotalVolumeChanged -= value; }
		}
		private void riseOnTotalVolumeChanged()
		{
			if (onTotalVolumeChanged != null) onTotalVolumeChanged(this, new EventArgs());
		}
		#endregion

		#region PublicFuncs
		/// <summary>
		/// Sets the Volume of Volume group by group name.
		/// </summary>
		/// <param name="groupName">Group Name.</param>
		/// <param name="val">Value.</param>
		public void SetVolume(string groupName, float val)
		{
			VolumeGroup targetVg = null;
			if (volumeGroups.TryGetValue(groupName, out targetVg)) {
				targetVg.Volume = val;
			}
		}

		///<summary>
		/// Returns Volume by volumeGroup.name; Returns -1 if not found group.
		/// </summary>
		public float GetVolume(string groupName)
		{
			VolumeGroup targetVg = GetVolumeGroup(groupName);
			return (targetVg != null) ? targetVg.Volume : -1;
		}

		/// <summary>
		/// Gets the Volume group by group name.
		/// </summary>
		/// <returns>The Volume group.</returns>
		/// <param name="groupName">Group Name.</param>
		public VolumeGroup GetVolumeGroup(string groupName)
		{
			VolumeGroup targetVg = null;
			volumeGroups.TryGetValue(groupName, out targetVg);
			return targetVg;
		}

		/// <summary>
		/// Fade the specified sound Volume from value to value during specified time and executes an action on end.
		/// </summary>
		/// <param name="sound">Sound.</param>
		/// <param name="fromVolume">From Volume.</param>
		/// <param name="toVolume">To Volume.</param>
		/// <param name="time">Time.</param>
		/// <param name="onFadeEnd">On fade end.</param>
		public void Fade(Sound sound, float fromVolume, float toVolume, float time, Action<Sound> onFadeEnd)
		{
			IEnumerator routine = null;
			if (fadeRoutines.TryGetValue(sound, out routine)) {
				StopCoroutine(routine);
				routine = fadeRoutine(sound, fromVolume, toVolume, time, onFadeEnd);
				fadeRoutines[sound] = routine;
			}
			else {
				routine = fadeRoutine(sound, fromVolume, toVolume, time, onFadeEnd);
				fadeRoutines.Add(sound, routine);
				sound.OnDestroyEvent += delegate (Sound snd)
				{
					if (fadeRoutines.ContainsKey(snd)) {
						fadeRoutines.Remove(snd);
					}
				};
			}
			sound.StartCoroutine(routine);
		}
		/// <summary>
		/// Fade the specified sound Volume from value to value during specified time.
		/// </summary>
		/// <param name="sound">Sound.</param>
		/// <param name="fromVolume">From Volume.</param>
		/// <param name="toVolume">To Volume.</param>
		/// <param name="time">Time.</param>
		/// <param name="onFadeEnd">On fade end.</param>
		public void Fade(Sound sound, float fromVolume, float toVolume, float time)
		{
			Fade(sound, fromVolume, toVolume, time, null);
		}
		/// <summary>
		/// Checks if the sound is fading by SoundMixer
		/// </summary>
		/// <param name="sound"></param>
		/// <returns></returns>
		public bool IsFading(Sound sound)
		{
			return (sound != null && fadeRoutines.ContainsKey(sound));
		}
		/// <summary>
		/// Breaks fading of sound.
		/// </summary>
		/// <param name="sound"></param>
		public void BreakFading(Sound sound)
		{
			IEnumerator routine = null;
			if (fadeRoutines.TryGetValue(sound, out routine)) {
				StopCoroutine(routine);
				fadeRoutines.Remove(sound);
			}
		}
		/// <summary>
		/// Smoothly switches from sound to sound.
		/// </summary>
		/// <returns>The to sound.</returns>
		/// <param name="fromSnd">From sound.</param>
		/// <param name="toSndName">To sound Name.</param>
		/// <param name="time">Time.</param>
		public Sound FadeToSoundParallels(Sound fromSnd, string toSndName, float time)
		{
			return FadeToSoundParallels(fromSnd, toSndName, time, time);
		}
		/// <summary>
		/// Smoothly switches from sound to sound.
		/// </summary>
		/// <returns>The to sound.</returns>
		/// <param name="fromSnd">From sound.</param>
		/// <param name="toSndName">To sound Name.</param>
		/// <param name="fadeOutTime">Fade out time.</param>
		/// <param name="fadeInTime">Fade in time.</param>
		public Sound FadeToSoundParallels(Sound fromSnd, string toSndName, float fadeOutTime, float fadeInTime)
		{
			Sound toSnd = SoundManager.Instance.PlaySound(toSndName);
			fadeBetweenSoundsParallel(fromSnd, toSnd, fadeOutTime, fadeInTime);
			return toSnd;
		}
		/// <summary>
		/// Fade out from one sound and fade in to another sound.
		/// </summary>
		/// <returns>The to sound.</returns>
		/// <param name="fromSnd">From sound.</param>
		/// <param name="toSndName">To sound Name.</param>
		/// <param name="time">Time.</param>
		public Sound FadeToSoundInSeries(Sound fromSnd, string toSndName, float time)
		{
			return FadeToSoundParallels(fromSnd, toSndName, time, time);
		}
		/// <summary>
		/// Fade out from one sound and fade in to another sound.
		/// </summary>
		/// <returns>The to sound.</returns>
		/// <param name="fromSnd">From sound.</param>
		/// <param name="toSndName">To sound Name.</param>
		/// <param name="fadeOutTime">Fade out time.</param>
		/// <param name="fadeInTime">Fade in time.</param>
		public Sound FadeToSoundInSeries(Sound fromSnd, string toSndName, float fadeOutTime, float fadeInTime)
		{
			Sound toSnd = SoundManager.Instance.PlaySound(toSndName);
			fadeBetweenSoundsInSeries(fromSnd, toSnd, fadeOutTime, fadeInTime);
			return toSnd;
		}
		#endregion

		#region PrivateFuncs
		private void fillVolumeGroups()
		{
			foreach (VolumeGroupInfo vgi in AudioLibrary.Instance.VolumeGroups) {
				VolumeGroup vg = (VolumeGroup)vgi;
				volumeGroups.Add(vg.Name, vg);
			}
		}

		private void fadeBetweenSoundsParallel(Sound fromSnd, Sound toSnd, float fadeOutTime, float fadeInTime)
		{
			if (toSnd != null) {
				Fade(toSnd, 0, toSnd.Volume, fadeInTime);
			}
			if (fromSnd != null) {
				Fade(fromSnd, fromSnd.Volume, 0, fadeOutTime, (sound) =>
				{
					sound.Stop();
				});
			}
		}

		private void fadeBetweenSoundsInSeries(Sound fromSnd, Sound toSnd, float fadeOutTime, float fadeInTime)
		{
			if (toSnd != null) toSnd.Pause();

			if (fromSnd != null) {
				Fade(fromSnd, fromSnd.Volume, 0, fadeOutTime, (sound) =>
				{
					sound.Stop();
					if (toSnd != null) {
						toSnd.Play();
						Fade(toSnd, 0, toSnd.Volume, fadeInTime);
					}
				});
			}
			else {
				if (toSnd != null) {
					toSnd.Play();
					Fade(toSnd, 0, toSnd.Volume, fadeInTime);
				}
			}
		}

		private IEnumerator fadeRoutine(Sound sound,
										float fromVolume,
										float toVolume,
										float time,
										Action<Sound> onEnd = null)
		{
			sound.Volume = fromVolume;

			float endTime = Time.unscaledTime + time;
			while (Time.unscaledTime <= endTime) {
				sound.Volume += (toVolume - fromVolume) / time * Time.unscaledDeltaTime;
				yield return new WaitForEndOfFrame();
			}
			sound.Volume = toVolume;

			fadeRoutines.Remove(sound);
			if (onEnd != null) onEnd(sound);
		}
		#endregion
	}
}