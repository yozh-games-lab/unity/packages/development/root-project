using UnityEngine;
using UnityEngine.Audio;
using YOZH.Core.CustomAttributes;

namespace YOZH.Plugins.SoundManager
{
	[System.Serializable]
	public class SoundInfo
	{
		[UniqueID(4, "0123456789", "Sound_")]
		public string SoundName = "";
		public string PoolName = typeof(AudioPool).ToString();
		public AudioClip Clip;
		[Range(0, 256)]
		public int Priority = 128;
		[Range(0, 1)]
		public float Volume = 0.5f;
		public string VolumeGroup = typeof(VolumeGroup).ToString();
		public bool Looped = false;
		public float StartTime = -1;
		public float EndTime = -1;
		public bool PauseOnTimeHold = true;
		public bool TimeDependent = false;
		[Range(-3, 3)]
		public float Pitch = 1;
		public AudioMixerGroup OutputMixer;

		///<summary>
		/// This will create a new gameObject
		/// </summary>
		public static explicit operator Sound(SoundInfo soundInfo)
		{
			return Sound.Create(soundInfo);
		}
	}
}