﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YOZH.Plugins.AStar
{
	public delegate void NodeEvent(Node sender);

	public class Node
	{
		public static event NodeEvent OnUpdated = (sender) => { };
		public const int MAX_COST = int.MaxValue;
		public const int MIN_COST = 1;

		public Bounds Bounds { get; private set; }

		public int CostFactor {
			get {
				int result = 0;
				obstacles.ForEach(obstacle => result += obstacle.Weight);
				return Mathf.Clamp(result, MIN_COST, MAX_COST);
			}
		}

		public bool IsWalkable {
			get {
				bool result = true;
				obstacles.ForEach(obstacle => result &= obstacle.IsWalkable);
				return result;
			}
		}

		public Node(Bounds position)
		{
			Bounds = position;
		}

		#region Neighbours
		private List<Node> neighbours = new List<Node>();
		public int NeighbourCount { get { return neighbours.Count; } }

		public Node GetNeighbour(int index)
		{
			return (index >= 0 && index < neighbours.Count) ? neighbours[index] : null;
		}
		public void AddHeighbour(Node node)
		{
			if (node == null || node == this) return;

			neighbours.Add(node);
			OnUpdated(this);
		}
		public void RemoveNeighbour(Node node)
		{
			if (node == null) return;

			neighbours.Remove(node);
			OnUpdated(this);
		}
		#endregion

		#region obstacles
		private List<ObstacleAgent> obstacles = new List<ObstacleAgent>();

		public ObstacleAgent GetObstacle(int index)
		{
			return (index >= 0 && index < obstacles.Count) ? obstacles[index] : null;
		}

		public void AddObstacle(ObstacleAgent obstacle)
		{
			if (obstacle == null) return;
			if (obstacles.Contains(obstacle)) return;

			obstacles.Add(obstacle);
			OnUpdated(this);
		}

		public void RemoveObstacle(ObstacleAgent obstacle)
		{
			if (!obstacles.Contains(obstacle)) return;

			obstacles.Remove(obstacle);
			OnUpdated(this);
		}
		#endregion


		public float GetHeuristicCost(Node from)
		{
			return Vector3.Distance(Bounds.center, from.Bounds.center) * CostFactor;
		}

	}
}