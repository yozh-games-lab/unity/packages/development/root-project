﻿using UnityEngine;
using System.Collections.Generic;

namespace YOZH.Plugins.AStar
{
	public class GridGraph
	{
		private Node[,,] nodes = new Node[0, 0, 0];
		private Bounds bounds = new Bounds();
		private Vector3 nodeSize = Vector3.zero;

		public int XSize { get { return nodes.GetLength(0); } }
		public int YSize { get { return nodes.GetLength(1); } }
		public int ZSize { get { return nodes.GetLength(2); } }
		public int Size { get { return nodes.Length; } }
		public int MaxNodeCost {
			get {
				int result = Node.MIN_COST;
				foreach (Node node in nodes) {
					if (node.IsWalkable && result < node.CostFactor) result = node.CostFactor;
				}
				return result;
			}
		}
		public Bounds Bounds { get { return bounds; } }

		public GridGraph(int xCount, int yCount, int zCount, Bounds bounds)
		{
			fillGrid(xCount, yCount, zCount, bounds);
		}

		public GridGraph(Vector3 nodeSize, Bounds bounds)
		{
			int xCount = (nodeSize.x > 0) ? (int)(bounds.size.x / nodeSize.x) : 0;
			int yCount = (nodeSize.y > 0) ? (int)(bounds.size.y / nodeSize.y) : 0;
			int zCount = (nodeSize.z > 0) ? (int)(bounds.size.z / nodeSize.z) : 0;


			Bounds actualBounds = new Bounds(bounds.center, new Vector3
			{
				x = nodeSize.x * xCount,
				y = nodeSize.y * yCount,
				z = nodeSize.z * zCount
			});

			fillGrid(xCount, yCount, zCount, actualBounds);
		}

		private void fillGrid(int xCount, int yCount, int zCount, Bounds bounds)
		{
			nodes = new Node[xCount, yCount, zCount];
			nodeSize = new Vector3
			{
				x = bounds.size.x / xCount,
				y = bounds.size.y / yCount,
				z = bounds.size.z / zCount,
			};
			this.bounds = bounds;

			for (int xIndex = 0; xIndex < xCount; xIndex++) {
				for (int yIndex = 0; yIndex < yCount; yIndex++) {
					for (int zIndex = 0; zIndex < zCount; zIndex++) {
						Vector3 nodePosition = new Vector3
						{
							x = bounds.min.x + bounds.size.x / xCount * xIndex + bounds.extents.x / xCount,
							y = bounds.min.y + bounds.size.y / yCount * yIndex + bounds.extents.y / yCount,
							z = bounds.min.z + bounds.size.z / zCount * zIndex + bounds.extents.z / zCount,
						};

						nodes[xIndex, yIndex, zIndex] = new Node(new Bounds(nodePosition, nodeSize));
					}
				}
			}

			createNodesConnections();
		}

		public Node GetNode(int xIndex, int yIndex, int zIndex)
		{
			return nodes[Mathf.Clamp(xIndex, 0, XSize), Mathf.Clamp(yIndex, 0, YSize), Mathf.Clamp(zIndex, 0, ZSize)];
		}
		public Node GetNode(Vector3 worldPosition)
		{
			GridPosition gridPoint = worldToGridPosition(worldPosition);
			return nodes[gridPoint.X, gridPoint.Y, gridPoint.Z];
		}

		public List<Node> GetIntersectedNodes(Bounds targetBounds, bool centerOnly = false)
		{
			GridPosition gridMin = worldToGridPosition(targetBounds.min);
			GridPosition gridMax = worldToGridPosition(targetBounds.max);

			List<Node> result = new List<Node>();

			for (int xIndex = gridMin.X; xIndex <= gridMax.X; xIndex++) {
				for (int yIndex = gridMin.Y; yIndex <= gridMax.Y; yIndex++) {
					for (int zIndex = gridMin.Z; zIndex <= gridMax.Z; zIndex++) {
						if (!centerOnly || targetBounds.Contains(nodes[xIndex, yIndex, zIndex].Bounds.center)) {
							result.Add(nodes[xIndex, yIndex, zIndex]);
						}
					}
				}
			}

			return result;
		}

		private void createNodesConnections()
		{
			for (int xIndex = 0; xIndex < nodes.GetLength(0); xIndex++) {
				for (int yIndex = 0; yIndex < nodes.GetLength(1); yIndex++) {
					for (int zIndex = 0; zIndex < nodes.GetLength(2); zIndex++) {

						for (int xNeighbourIndex = (xIndex > 0) ? xIndex - 1 : 0;
							 xNeighbourIndex < nodes.GetLength(0) && xNeighbourIndex <= xIndex + 1;
							 xNeighbourIndex++) {

							for (int yNeighbourIndex = (yIndex > 0) ? yIndex - 1 : 0;
								 yNeighbourIndex < nodes.GetLength(1) && yNeighbourIndex <= yIndex + 1;
								 yNeighbourIndex++) {

								for (int zNeighbourIndex = (zIndex > 0) ? zIndex - 1 : 0;
									 zNeighbourIndex < nodes.GetLength(2) && zNeighbourIndex <= zIndex + 1;
									 zNeighbourIndex++) {

									nodes[xIndex, yIndex, zIndex].AddHeighbour(nodes[xNeighbourIndex, yNeighbourIndex, zNeighbourIndex]);
								}
							}
						}

					}
				}
			}
		}

		private GridPosition worldToGridPosition(Vector3 worldPosition)
		{
			return new GridPosition
			{
				X = Mathf.Clamp((int)((worldPosition.x - bounds.min.x) / nodeSize.x), 0, nodes.GetLength(0) - 1),
				Y = Mathf.Clamp((int)((worldPosition.y - bounds.min.y) / nodeSize.y), 0, nodes.GetLength(1) - 1),
				Z = Mathf.Clamp((int)((worldPosition.z - bounds.min.z) / nodeSize.z), 0, nodes.GetLength(2) - 1)
			};
		}

		private struct GridPosition
		{
			public int X;
			public int Y;
			public int Z;
		}
	}
}