﻿using UnityEngine;
using System;
using YOZH.Plugins.SoundManager;


namespace YOZH.Plugins.Visualization
{
	[Serializable]
	public class ActorVisualizer
	{
		[SerializeField]
		private AnimatorDataSet[] animationTriggers = new AnimatorDataSet[0];

		[SerializeField]
		private SoundEvent sound = new SoundEvent();

		[SerializeField]
		private ParticleSystem particleSystem = null;

		public void Execute(Animator animatorController)
		{
			executeAnimation(animatorController);
			sound.Execute();
			executeParticles();
		}
		public void Break(Animator animatorController)
		{
			breakAnimation(animatorController);
			sound.Break();
			breakParticles();
		}
		public bool IsFinished(Animator animatorController)
		{
			return IsFinishedAnimation(animatorController) && IsFinishedParticles() && IsFinishedSound();
		}
		public bool IsFinishedAnimation(Animator animatorController)
		{
			foreach (AnimatorDataSet trigger in animationTriggers) {
				if (!trigger.IsFinished(animatorController)) {
					return false;
				}
			}
			return true;
		}
		public bool IsFinishedSound()
		{
			return sound.IsFinished();
		}
		public bool IsFinishedParticles()
		{
			return particleSystem == null || !particleSystem.IsAlive();
		}

		private void executeParticles()
		{
			if (particleSystem != null) {
				particleSystem.Play(true);
			}
		}
		private void breakParticles()
		{
			if (particleSystem != null) {
				particleSystem.Stop(true);
			}
		}
		private void executeAnimation(Animator animatorController)
		{
			foreach (AnimatorDataSet trigger in animationTriggers) {
				trigger.Execute(animatorController);
			}
		}
		private void breakAnimation(Animator animatorController)
		{
			foreach (AnimatorDataSet trigger in animationTriggers) {
				trigger.Break(animatorController);
			}
		}

	}
}