﻿using UnityEditor;

namespace YOZH.Plugins.Parallax.Editor
{
	[CustomEditor(typeof(ParallaxSprite)), CanEditMultipleObjects]
	public class ParallaxSpriteEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			ParallaxSprite src = (ParallaxSprite)target;

			if (src.CurrentTranslateMode == ParallaxSprite.TranslateMode.Scale || src.CurrentTranslateMode == ParallaxSprite.TranslateMode.PositionAndScale) {
				src.CurrentScaleMode = (ParallaxSprite.ScaleMode)EditorGUILayout.EnumPopup("Scale Mode", src.CurrentScaleMode);
			}
			if (src.CurrentTranslateMode == ParallaxSprite.TranslateMode.PositionAndScale) {
				src.ScaleSpeedFactor = EditorGUILayout.FloatField("Scale Speed Factor", src.ScaleSpeedFactor);
				src.PositionSpeedFactor = EditorGUILayout.FloatField("Position Speed Factor", src.PositionSpeedFactor);
			}
			else {
				src.ScaleSpeedFactor = 1;
				src.PositionSpeedFactor = 1;
			}
		}
	}
}