﻿using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.Parallax
{
	public class ParallaxGroup : MonoBehaviour
	{
		[SerializeField]
		protected TranslateDirection translateDirection = TranslateDirection.Normal;
		[SerializeField]
		protected List<ParallaxLayer> layers = new List<ParallaxLayer>();
		[SerializeField]
		protected bool translateInactive = false;
		[SerializeField]

		private void Awake()
		{
			if (layers.Count == 0) {
				FindLayers();
			}
		}

		[ContextMenu("Find layers")]
		public void FindLayers()
		{
			layers.Clear();
			foreach (ParallaxLayer layer in this.transform.GetComponentsInChildren<ParallaxLayer>(true)) {
				if (layer.transform.parent == this.transform) {
					layers.Add(layer);
					layer.FindLayers();
				}
			}
		}

		[ContextMenu("Clear layers")]
		public void ClearLayers()
		{
			foreach (ParallaxLayer layer in this.transform.GetComponentsInChildren<ParallaxLayer>(true)) {
				if (layer.transform.parent == this.transform) {
					layer.ClearLayers();
				}
			}
			layers.Clear();
		}

		public virtual void Translate(Vector2 delta)
		{
			foreach (ParallaxLayer layer in layers) {
				if (translateInactive || layer.gameObject.activeSelf) layer.Translate(delta * (int)translateDirection);
			}
		}

		public enum TranslateDirection
		{
			Inverce = -1,
			Normal = 1
		}
	}
}