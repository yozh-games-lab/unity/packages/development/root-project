﻿using UnityEngine;

namespace YOZH.Plugins.Parallax
{
	public class ParallaxMotor : MonoBehaviour
	{
		public ParallaxGroup ParallaxGroup;
		public Vector2 MoveVector = Vector2.one;

		void Awake()
		{
			if (ParallaxGroup == null) ParallaxGroup = this.gameObject.GetComponent<ParallaxGroup>();
		}

		void Update()
		{
			ParallaxGroup.Translate(MoveVector * Time.deltaTime);
		}
	}
}