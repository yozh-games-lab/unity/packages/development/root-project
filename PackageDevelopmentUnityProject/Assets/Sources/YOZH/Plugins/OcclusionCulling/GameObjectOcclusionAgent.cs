﻿using UnityEngine;
using System.Collections;

namespace YOZH.Plugins.OcclusionCulling
{
	public class GameObjectOcclusionAgent : MonoBehaviour, IOcclusionAgent
	{
		[SerializeField]
		private Vector3 boundsSize = Vector3.one;
		private Bounds thisBounds;

		private void Awake()
		{
			thisBounds = new Bounds(transform.position, boundsSize);
			OcclusionTree.RegisterAgent(this);
		}

		private void OnDestroy()
		{
			OcclusionTree.UnregisterAgent(this);
		}

		#region IOcclusionAgent implementation
		public void SetVisible()
		{
			gameObject.SetActive(true);
		}
		public void SetInvisible()
		{
			gameObject.SetActive(false);
		}
		public Bounds Bounds { get { return thisBounds; } }
		public bool IsVisible { get { return gameObject.activeSelf; } }
		public bool IsStatic { get { return gameObject.isStatic; } }
		#endregion
	}
}